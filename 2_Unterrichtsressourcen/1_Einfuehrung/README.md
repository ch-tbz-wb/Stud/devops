# Devops

[TOC]
## Was ist Devops?
Brainstorming an der Wandtafel (10-15min)

## Warum Devops? 

Ziel des heutigen Unterrichts ist es euch klar zu machen warum man DevOps einführen sollte, d.h. wir werden die Probleme des klassischen Software Development Livecycles anschauen.

### Auftrag 1: Lektüre zu Projektmanagement

Überfliegt zum Einstieg folgende Webseiten:

**eher Klassisch**
- https://blog.mindmanager.com/de/die-5-phasen-des-projektmanagementprozesses/

**eher Agil**
-  https://agilescrumgroup.de/beispiele-agiles-arbeiten/

**was ist überhaupt Agile Softwareentwicklung**
-  https://agilemanifesto.org/iso/de/manifesto.html

### Probleme des Projektmanagement in der IT

Bei IT-Projekten kommt es immer wieder zu folgenden Problemen:

  - Ungenaue Anforderungern bzw. immer neue Anforderungen
  - Kommunikation (Missverständnisse) 
  - Projektverzögerungen (langsames Vorwärtskommen durch Abhängigkeiten von anderen Teams oder Personen) 
  - Angst vor dem Go-Live (man ist sich nicht ganz sicher ob es funktionieren wird) 
  - Integration von veralteter Technologie
  - keine oder wenig Automatisierung 
  - Resourcenmangel (meistens Personalmangel)
  - unverhorgesehene Probleme treten auf
  - ..
  
### Auftrag 2: Gruppenarbeit zu zweit

Jeder überlegt sich kurz ein Projekt (in seinem Betrieb) bei dem es zu solchen Problemen kam. 
Der Buddy macht dann ein Interview und Notizen dazu und versucht durch Fragen herauszufinden, wo genau die Probleme lagen und wie sie gelöst wurden oder eben auch nicht.

 -> Zusammentragen und die wichtigsten Probleme aufschreiben und den Lösungsansätzen im Plenum


### Auftrag 3: Was müsste man ändern?

Bildet 3er-Gruppenarbeit. Diskutiert die Frage was man ändern müsste um die oben genannten häufigsten Probleme anzupacken. Jede Gruppe geht 1-3 Themen an

wie z.B: 
  - Verkürzung der Lead-Times durch Rollouts von "kleinen" Changes

- Zusammentragen der Ansätze
    
## Wie helfen Tools? 

### Auftrag 4: Tools als Hilfsmittel
Gruppenarbeit zu tritt (20 min) zum Thema wie Tools helfen. 

Überlegt euch bei welchen Änderungen Tools helfen können und wie ganau.

Nehmt 2-3 Tools die ihr "kennt" und überlegt euch wie sie helfen könnten.

z.B. Unit-Testing von Software -> Automatische Fehlerreports -> weniger unvorhergesehenes beim Software Deployment und schnellere Software Deployments möglich -> keine Angst vor Code Changes -> Mehr Risiko möglich -> kleinere Features schneller werden ausgerollt. -> ...

Zusammentragen der Tools

## Was ist Devops? (zweiter Anlauf)

  
### Auftrag 5: Was ist Devops und Grundprinzipien von Devops

Lest im Buch DevOps von Reihnwerk folgende  Seiten
- Kapitel 2 "Was ist DevOps?" (Seite 23-25) 

Überfliegt die Kernaussagen des
["Phoenix Projekts" der Bibel des DevOps](https://github.com/keyvanakbary/learning-notes/blob/master/books/the-phoenix-project.md). Versucht noch nicht alles zu verstehen. Wir werden im weiteren noch darauf eingehen.

Diskussion

### Auftrag 6:

Gruppenarbeit zu zweit:
Schreibt auf was an diesen Aussagen falsch ist? 
  - **Wir müssen diesen Prozess nur automatisieren, dann brauchen wir einen Adminstrator weniger und sind erst noch schneller**
  - **Wenn wir die Software automatisch ausrollen laufen wir Gefahr fehlerhaft Software im Betrieb zu haben**
  - **Die Entwickler haben wieder mal schlechten Code geliefert der im Betrieb unbrauchbare logs produziert. Jetzt müssen wir diese mühsam Auswerten**
  - **Das nächste Change-Window ist in 3 Wochen. Bis dahin dürfen wir keine neuen Features ausrollen**
  - **Der Pull-Request muss noch vom Verantwortlichen (welcher keinen Plan hat was drin steht) genehmigt werden**
  - **Unsere Code-Repos sollten nur von unserem Team angeschaut werden können. Das erhöht die Sicherheit.**
  - **Es gibt keine Dokumentation der Schnittstelle. Darum kann ich nicht weiterarbeiten**
  - **Bis der Code fertig ist, kann ich nicht mit den Tests beginnen**

Überlegt euch noch ein zwei Aussagen, die ihr immer wieder gehört habt, die wahrscheinlich auch falsch sind.
