[TOC]

# MVP (Minimum Viable Product)

Ein **Minimum Viable Product (MVP)** ist eine Entwicklungsstrategie, bei der ein Produkt in seiner einfachsten Form erstellt wird, um es schnell auf den Markt zu bringen und direktes Feedback von den Nutzern zu sammeln. Das Ziel des MVP ist es, die Grundidee zu testen, bevor viel Zeit und Geld in die vollständige Entwicklung investiert wird. 

## Ausführliche Merkmale eines MVP:

1. **Minimaler Funktionsumfang**: Das MVP hat nur die grundlegenden Funktionen, die notwendig sind, um das Hauptproblem der Zielgruppe zu lösen. Es ist nicht perfekt und hat noch keine fortgeschrittenen Features oder eine vollständige Benutzeroberfläche. Der Fokus liegt darauf, dass es nutzbar ist und seinen Zweck erfüllt.
   
2. **Frühzeitiger Marktstart**: Durch die Reduzierung auf die wesentlichen Funktionen kann das Produkt schneller auf den Markt gebracht werden. Dies ermöglicht es den Entwicklern, so früh wie möglich echtes Feedback zu sammeln.

3. **Risikominimierung**: Statt Monate oder Jahre in die vollständige Entwicklung eines Produkts zu investieren, kann das MVP helfen, das Risiko eines Flops zu minimieren. Wenn das MVP nicht auf positive Resonanz stößt, ist der Verlust von Zeit und Geld geringer, da nicht alle Features und Verfeinerungen integriert wurden.

4. **Schnelles Feedback und Iteration**: Der wichtigste Aspekt eines MVP ist das Feedback der Nutzer. Sobald das MVP veröffentlicht ist, wird anhand des Nutzerverhaltens und der Rückmeldungen analysiert, ob das Produkt den Bedürfnissen entspricht oder wo Anpassungen erforderlich sind. Auf dieser Grundlage kann das Produkt schrittweise verbessert und weiterentwickelt werden.

5. **Kosten- und Ressourcenschonend**: Da das MVP nur minimale Ressourcen in der Anfangsphase benötigt, können Unternehmen ihre Zeit und ihr Geld auf die Kernfunktionen konzentrieren und später entscheiden, welche zusätzlichen Funktionen wirklich notwendig sind.

## Senkrechter vs. Horizontaler Ansatz bei einem MVP:

Ein entscheidender Aspekt bei der Entwicklung eines MVP ist, dass das Produkt **senkrecht** statt **horizontal** entwickelt wird.

![](../../x_gitressourcen/MVP-Minimum-Viable-Product-Characteristics-Lean-Product-Management.jpg)

- **Horizontaler Ansatz**: Beim horizontalen Ansatz wird nur eine Dimension des Produkts vollständig entwickelt, zum Beispiel die **Funktionalität**. Dabei könnten andere wichtige Aspekte wie **Zuverlässigkeit**, **Benutzerfreundlichkeit** oder **Kundenzufriedenheit** vernachlässigt werden. Das Ergebnis wäre ein Produkt, das technisch zwar funktioniert, aber möglicherweise nicht stabil genug ist, schwer zu bedienen ist oder die Erwartungen der Kunden nicht erfüllt. Dies kann zu einer schlechten Nutzererfahrung führen.

- **Senkrechter Ansatz**: Im Gegensatz dazu wird das MVP bei einem senkrechten Ansatz so entwickelt, dass alle wichtigen Dimensionen – **Funktionalität**, **Zuverlässigkeit**, **Benutzerfreundlichkeit** und **Kundenzufriedenheit** – auf einem grundlegenden Niveau abgedeckt sind. Dadurch bietet das Produkt eine **minimale, aber vollständige Erfahrung**, die das gesamte Nutzererlebnis widerspiegelt, auch wenn es in einfacher Form gehalten ist.

Ein MVP, das nach dem senkrechten Ansatz entwickelt wird, zeichnet sich dadurch aus, dass es:
- **Funktional** ist: Es erfüllt seine Hauptaufgabe und löst das Kernproblem der Nutzer.
- **Zuverlässig**: Es läuft stabil und ohne schwerwiegende technische Fehler.
- **Benutzbar**: Die Benutzeroberfläche ist intuitiv und leicht verständlich, sodass eine einfache Nutzung ohne große Einarbeitung möglich ist.
- **Zufriedenstellend**: Die Nutzererfahrung ist insgesamt positiv, selbst wenn das Produkt noch nicht alle gewünschten Funktionen enthält.

Dieser senkrechte Ansatz stellt sicher, dass das MVP nicht nur die technische Funktionalität testet, sondern auch, wie das Produkt in seiner Gesamtheit bei den Nutzern ankommt. Dadurch wird umfassendes Feedback gewonnen, das zeigt, ob das Produkt in allen relevanten Bereichen den Bedürfnissen der Zielgruppe gerecht wird.

### Beispiel:

Stell dir vor, du entwickelst eine Second-Hand-Kleidungs-App. In einem senkrechten MVP würden alle Kernkomponenten enthalten sein:
- **Funktionalität**: Die Nutzer können Kleidungsstücke einstellen und nach Artikeln suchen.
- **Zuverlässigkeit**: Die App läuft stabil, ohne Abstürze oder technische Fehler.
- **Benutzbarkeit**: Die App hat ein einfaches Interface, das auch ohne lange Einarbeitung verständlich ist.
- **Kundenzufriedenheit**: Das gesamte Erlebnis ist für den Nutzer positiv, auch wenn nicht alle möglichen Features verfügbar sind.

Dieses vollständige, aber einfache Produkt kann dann durch Nutzerfeedback stetig verbessert und erweitert werden. Der **senkrechte Ansatz** stellt sicher, dass alle kritischen Aspekte eines Produkts berücksichtigt werden, sodass das MVP direkt auf den Markt und von den Nutzern verwendet werden kann.

## Warum ist ein MVP wichtig?

Ein MVP spart Zeit und Geld, indem es den Fokus auf das Wesentliche legt und sofortiges Feedback vom Markt erhält. Anstatt zu spekulieren, welche Features gebraucht werden, basiert die Weiterentwicklung des Produkts auf echten Daten und Nutzerfeedback. Es hilft zu verstehen, ob die Idee Potenzial hat und wie sie verbessert werden kann, bevor zu viele Ressourcen in eine möglicherweise falsche Richtung fließen.

Das MVP ist ein grundlegender Ansatz in der modernen Produktentwicklung, insbesondere in Bereichen wie Startups, wo Ressourcen oft begrenzt sind, und schnelle Markttests entscheidend für den Erfolg sein können.

---
Quellen: </br>
https://t2informatik.de/wissen-kompakt/minimum-viable-product/</br>
https://www.humanizingwork.com/the-humanizing-work-guide-to-splitting-user-stories/
