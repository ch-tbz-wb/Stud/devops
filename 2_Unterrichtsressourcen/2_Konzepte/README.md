# Inhalt der Kompetenz A

[TOC]

## three ways to implement DevOps
Die "Three Ways" des DevOps, eingeführt von Gene Kim in "The Phoenix Project", bieten einen konzeptionellen Rahmen für die Prinzipien und Praktiken, die erfolgreiche DevOps-Transformationen vorantreiben.
Die "Three Ways" im DevOps-Prinzip umfassen Systemdenken, Verstärkung von Feedback-Schleifen und die Kultur der kontinuierlichen Experimentation und des Lernens.
Systemdenken betont die Optimierung des gesamten Systems statt einzelner Teile, um den Arbeitsfluss von Entwicklung bis zur Bereitstellung zu verbessern.
Verstärkung von Feedback-Schleifen konzentriert sich auf schnelle, effektive Rückmeldungen, um Probleme frühzeitig zu erkennen und zu beheben, was die Produktqualität steigert und Ausfallzeiten reduziert.
Die Kultur der kontinuierlichen Experimentation und des Lernens ermutigt Teams, neue Ideen auszuprobieren, Risiken einzugehen und aus Erfahrungen zu lernen, was kontinuierliche Verbesserung und Innovationsfähigkeit fördert.

In diesem Auftrag lernt ihr auf praktische Weise, was three ways bedeutet und wie sie in der Praxis umgesetzt werden können.

[three ways to implement DevOps](../5_Aufträge/3ways.md)

## four types of work
Die "Four Types" im DevOps-Kontext bezeichnen verschiedene Arten von Workloads, die unterschiedliche Anforderungen an Zuverlässigkeit, Flexibilität und Skalierbarkeit haben: Geschäftskritische Anwendungen, Entwicklungs- und Testumgebungen, Betriebs- und Produktionsumgebungen sowie unterstützende Dienste und Tools. Diese Einteilung hilft, spezifische DevOps-Praktiken gezielt auf die Bedürfnisse jeder Workload anzupassen und eine effiziente Bereitstellung sicherzustellen.

In diesem Auftrag lernt ihr die vier Arten von Workloads kennen und erfahrt, wie sie in der Praxis unterschiedlich behandelt werden.

[four types of work](../5_Aufträge/4typesOfWork.md)

## CALMS-Prinzipien
CALMS repräsentiert fünf grundlegende Prinzipien im DevOps: Kultur, Automatisierung, Lean, Messung und Teilen. Diese Prinzipien fördern Zusammenarbeit, Automatisierung, Prozessoptimierung, Leistungsmessung und Wissensaustausch, um die Effizienz und Agilität in der Softwareentwicklung und Bereitstellung zu verbessern.

In diesem Auftrag lernt ihr die CALMS-Prinzipien kennen und erfahrt, wie sie die Zusammenarbeit und Effizienz in DevOps-Teams fördern.

[CALMS-Prinzipien](../5_Aufträge/CALMS.md)


## Code-Repository
In der modernen Softwareentwicklung sind effiziente Versionierungssysteme und strukturierte Workflows unerlässlich, um Qualität, Zusammenarbeit und die pünktliche Auslieferung von Projekten zu gewährleisten. Besonders in Projekten mit einer komplexen Codebasis und einem großen Entwicklerteam, wie dem der “Metallbau Müller GmbH”, wird die Implementierung eines geeigneten Softwareentwicklungsprozesses entscheidend. In diesem Kontext spielt das Git-Versionierungssystem eine zentrale Rolle, um die Zusammenarbeit zwischen internen Entwicklern und externen Beratern effizient zu gestalten.

In dieser Fallstudie und dem dazugehörigen Auftrag lernt ihr, wie man passende Workflows und Versionierungssysteme für Unternehmen erstellen und praktisch nutzen kann.

[Code-Repository](../5_Aufträge/Code-Repository.md)

## SDCL vs. DevOps lifecycle
Die Softwareentwicklung und Bereitstellung durchläuft verschiedene Phasen, die in einem Software Development Lifecycle (SDLC) oder DevOps Lifecycle organisiert sind. Während der SDLC traditionell in sequentiellen Schritten wie Planung, Entwicklung, Testen und Bereitstellung organisiert ist, betont der DevOps Lifecycle die kontinuierliche Integration, Bereitstellung und Überwachung von Software, um die Time-to-Market zu verkürzen und die Produktqualität zu verbessern.

In diesem Auftrag lernt ihr die Unterschiede zwischen SDLC und DevOps Lifecycle kennen und erfahrt, wie ein Unternehmen wie TechWave Solutions von einem traditionellen SDLC-Ansatz zu einem DevOps-Ansatz übergehen kann.

[SDLC vs. DevOps Lifecycle](../5_Aufträge/SDLC%20vs%20devops%20live%20cycle.md)

## MVP (Minimum Viable Product)
Ein **MVP** ist eine Entwicklungsstrategie, bei der ein Produkt in seiner einfachsten Form erstellt wird, um es schnell auf den Markt zu bringen und direktes Feedback von den Nutzern zu sammeln. Das Ziel des MVP ist es, die Grundidee zu testen, bevor viel Zeit und Geld in die vollständige Entwicklung investiert wird.

[Minimum Viable Product](Minimum%20Viable%20Product.md)

## Agile Vorgehensweise, Agile Manifesto und die agilen Werte & Prinzipien
Agile Werte und Prinzipien bilden die Grundhaltung für agile Praktiken und Methoden. Ob diese Werte und Prinzipien tatsächlich gelebt werden, hängt von jedem Einzelnen ab, da es eine bewusste Entscheidung ist, sie in das eigene Verhalten zu integrieren.

[Agile Vorgehensweise - Agile Manifesto - Agilen Werte und Prinzipien](Agile%20Vorgehensweise.md)