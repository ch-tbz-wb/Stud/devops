# **Lösungen und Antworten für die Aufgabenstellung:**

## Teil 1 - Methodik: 

### Frage 2: 
Überlegt euch welche Branching Strategie für die Metallbau Müller GmbH geeignet sein könnte.

---
### Musterlösung Frage 3:

- Die **GitFlow-Branching-Strategie** bietet die nötige Struktur und Skalierbarkeit für ein grösseres Team und sorgt für Stabilität bei der Softwareentwicklung.

#### Begründung: 
**Vorschlag: GitFlow**
Für die **Metallbau Müller GmbH** empfiehlt sich die **GitFlow**-Branching-Strategie. Diese Strategie ist besonders für grössere Teams und Projekte geeignet, da sie eine klare Struktur und Organisation für den Entwicklungsprozess bietet.

**Beschreibung von GitFlow**:
- **Master-Branch**: Enthält stets den stabilen, produktionsreifen Code.
- **Develop-Branch**: Hier wird die laufende Entwicklung durchgeführt.
- **Feature-Branches**: Für jede neue Funktion wird ein eigener Branch von „develop“ erstellt. Nach Abschluss der Arbeit wird dieser in „develop“ gemerged.
- **Release-Branches**: Wenn eine Version zur Veröffentlichung vorbereitet wird, entsteht ein Release-Branch, der von „develop“ ausgeht. Hier erfolgen Tests und Bugfixes vor dem finalen Merge in „master“ und „develop“.
- **Hotfix-Branches**: Für dringende Fehlerbehebungen wird ein eigener Hotfix-Branch direkt vom „master“ erstellt.

**Detaillierte Begründung**:
- **Klarheit und Struktur**: GitFlow bietet eine klare Trennung zwischen stabilen Versionen und der laufenden Entwicklung. Dies ist wichtig, da Metallbau Müller eine **optimierte Fertigungssoftware** entwickelt, bei der Stabilität und Qualitätssicherung im Fokus stehen.
- **Skalierbarkeit**: Da das Team aus **50 Entwicklern** besteht, hilft GitFlow, parallele Entwicklungen durch Feature-Branches zu organisieren, ohne die Stabilität des Hauptcodes zu gefährden.
- **Release-Management**: Die Nutzung von Release-Branches erlaubt es, den Code für Produktionsfreigaben systematisch zu stabilisieren und zu testen, was in einem Fertigungsumfeld essenziell ist.

**Pro & Kontra von GitFlow**:

| **Pro** | **Kontra** |
|---------|------------|
| Gute **Strukturierung** des Prozesses mit klaren Branch-Rollen | Eher **komplex** für kleinere Projekte oder Teams |
| Erleichtert die **Versionierung** und die Integration neuer Funktionen | Erfordert strenge **Disziplin** im Team |
| Unterstützt **parallele Entwicklung** und Bugfixes durch getrennte Branches | Kann durch viele Branches und Merges den **Workflow verlangsamen** |
| Hohe **Sicherheit** durch isolierte Entwicklungsstufen (Develop, Release, Master) | Bedarf an einem **klaren Prozess** zur Vermeidung von Fehlern in der Nutzung von Branches |


-----


### Frage 3: Macht euch Gedanken zu folgenden Punkten und begründet eure Entscheidungen wo nötig:

* Welche Versionierungsstrategie ist in welchen Fällen sinnvoll?
* Schlagt Ihr der Firma Metallbau Müllber GmbH ein Single oder Multi-Repo Ansatz vor?
* Welche Empfehlung gebt Ihr betreffend die Versionsnummern?
* Welche Tools sollen die Entwickler nutzen um miteinander arbeiten zu können?

---

### Musterlösung Frage 3:

- Der **Single-Repo-Ansatz** ist besser geeignet für die Verwaltung eines einheitlichen Produkts und vereinfacht die Zusammenarbeit und den Workflow, besonders für ein Team, das noch wenig Erfahrung mit Versionskontrolle hat.


**Vorschlag: Single-Repository-Ansatz**
Für die **Metallbau Müller GmbH** wird ein **Single-Repo-Ansatz** empfohlen. Da die Software für ein einzelnes, spezifisches Produkt (Fertigungsprozessoptimierung) entwickelt wird, erscheint es sinnvoll, den gesamten Quellcode in einem einzigen Repository zu verwalten.

**Begründung**:
- **Einheitliches Produkt**: Die Software dient der **Optimierung von Fertigungsprozessen** und wird als ein einziges Produkt verwendet. Es gibt keine Unterprojekte, die strikt voneinander getrennt werden müssen.
- **Einfache Verwaltung**: Ein einzelnes Repository reduziert die Komplexität bei der Verwaltung von Branches und Abhängigkeiten. Dies ist besonders hilfreich, wenn die Entwickler bisher **keine Erfahrung mit Versionskontrolle** haben.
- **Kollaboration**: Ein Single-Repo fördert die **Zusammenarbeit** und ermöglicht einen einfachen Überblick über den gesamten Code. Es wird vermieden, dass Entwickler zwischen verschiedenen Repos hin- und herwechseln müssen.

- **Weniger Overhead**: Der administrative Aufwand ist geringer, da nur ein Repository gepflegt werden muss. Dadurch werden mögliche Fehlerquellen durch das Trennen von Projekten reduziert.

**Pro & Kontra des Single-Repo-Ansatzes**:

| **Pro** | **Kontra** |
|---------|------------|
| **Einfachere Verwaltung** von Branches und Code | Mögliche **Leistungsprobleme** bei sehr grossen Projekten |
| **Weniger Kommunikationsaufwand** zwischen Teams | Risiko von **Merge-Konflikten** bei sehr vielen parallelen Arbeiten |
| Fördert **Zusammenarbeit** und Überblick über den gesamten Code | **Einschränkungen** bei Teams, die unterschiedliche Teile der Software unabhängig voneinander entwickeln wollen |
| Geringerer **administrativer Aufwand** | Für stark modulare Software könnte eine **Entkopplung** der Projekte sinnvoll sein |

**Alternativüberlegung: Multi-Repo Ansatz**
In bestimmten Szenarien, z.B. bei einer starken Modularisierung der Software oder der Notwendigkeit, unabhängige Teams zu haben, könnte ein **Multi-Repo-Ansatz** sinnvoll sein. Dies wäre jedoch für Metallbau Müller nicht ideal, da es zu viel Komplexität für die Entwickler einführen würde, die bisher lokal und ohne Versionskontrolle gearbeitet haben.

**Versionsnummer**  
Semantic versioning verwenden - Mayor, Minor, Patch. Gängiges Modell.

----


**Vorschlag zu den Tools:**

Entwicklerteams sollten eine Kombination folgender Tools verwenden, um effektiv zusammenzuarbeiten:

1.	Code-Repository (z.B. Git, Azure DevOps): Ermöglicht die Versionskontrolle und den Zugriff auf den aktuellen Code.
2.	Issue-Tracking-System (z.B. Jira, GitHub Issues): Verknüpft Entwicklungsaufgaben mit User Stories und stellt sicher, dass der Fortschritt nachvollziehbar ist.
3.	CI/CD-Tools (z.B. Jenkins, GitLab CI): Automatisieren das Testen, Bauen und Bereitstellen des Codes.
4.	Kommunikationstools (z.B. Slack, Microsoft Teams): Sorgen für den regelmäßigen Austausch und die Synchronisation im Team.
5.	Code Review Tools (z.B. Pull Requests in GitHub, Azure DevOps): Unterstützen bei der Überprüfung von Code durch Kollegen vor dem Merge in die Hauptzweige.

**Begründung:**

Diese Tools bilden das Rückgrat moderner Softwareentwicklungsprozesse und ermöglichen es Teams, effizient und strukturiert zu arbeiten. Gerade in einem großen Team wie bei der „Metallbau Müller GmbH“, wo viele Entwickler an unterschiedlichen Teilen der Codebasis arbeiten, ist eine strukturierte Kollaboration entscheidend. Tools wie Git in Kombination mit CI/CD sorgen für eine reibungslose Integration und verhindern Konflikte oder Code-Qualitätsprobleme.

**Pro und Kontra:**

| **Tool**                         | **Pro**                                                                                         | **Kontra**                                                                                     |
|-----------------------------------|-------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------|
| **Code-Repository (Git, Azure DevOps)** | - Versionskontrolle und Rückverfolgbarkeit.<br> - Unterstützung für Branching-Strategien.<br> - Schutz vor Datenverlust und paralleles Arbeiten. | - Einarbeitung notwendig, besonders bei komplexen Branching-Strategien.<br> - Potenzielle Merge-Konflikte bei großen Teams. |
| **Issue-Tracking-System (Jira, GitHub Issues)** | - Transparente Aufgabenverteilung und Fortschrittsverfolgung.<br> - Verknüpfung von Commits mit User Stories oder Bugs. | - Kann zeitaufwändig sein, wenn es nicht effizient verwendet wird.<br> - Erfordert gute Planung für Effektivität. |
| **CI/CD-Tools (Jenkins, GitLab CI)** | - Automatisiert Builds, Tests und Deployments.<br> - Schnellere Fehlererkennung und -behebung. | - Komplexes Set-up und Wartung.<br> - Hoher initialer Konfigurationsaufwand. |
| **Kommunikationstools (Slack, Microsoft Teams)** | - Schnelle Absprachen und asynchrone Kommunikation.<br> - Integration mit anderen Tools möglich. | - Gefahr von Informationsüberflutung.<br> - Ablenkungspotential durch ständige Benachrichtigungen. |
| **Code Review Tools (Pull Requests, GitHub, Azure DevOps)** | - Fördert die Codequalität durch Peer Reviews.<br> - Diskussion und Kommentierung direkt am Code möglich. | - Kann den Entwicklungsprozess verlangsamen, wenn zu viele offene Pull Requests bestehen.<br> - Erfordert Disziplin bei der Anwendung von Coding-Standards. |
