# Inhalt der Kompetenz A

[[_TOC_]]

*Ersetzen mit einer allgemeinen Beschreibung mit evtl. vorhandenem Big Picture oder Einführungsfilm*

## Unterthema 1

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*

## Unterthema 2

*Ersetzen mit einer allgemeinen Beschreibung mit einem Image oder Einführungsfilm*

### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*### Kompetenzen

*Aufzählen der Handlungskompetenzen die in dieser Kompetenz behandelt werden, z.B. A2.3, B5.1*

### Lernziele / Taxonomie 

*Ersetzen mit einer Liste von Inhalten welche durchgenommen werden*

### Transfer

### Hands-on

*Verweise auf Unterverzeichnis mit Hands-on*

### Links

*Links zu den Unterthemen*

## Links

*Allgemeine Links welche sich nicht in die Unterthemen einordnen lassen*

