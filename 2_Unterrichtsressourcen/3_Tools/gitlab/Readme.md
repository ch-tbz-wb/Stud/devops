# Lerning Path 

Folgender [Link](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/tree/main) führt zu einem Step by Step Kurs um eine GitLab-Pipeline aufzubauen.

Im [Video](https://www.youtube.com/watch?v=PGyhBwLyK2U) wird alles sehr genau erklärt.

Für mich waren die [Course Notes](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md?ref_type=heads) genügend, damit ich nachvollziehen konnte was im Kurs geschieht. 

Dieser Kurs wird in den nächsten 8 Stunden benutzt um Gitlab nochmals ein bisschen besser kennzulernen.

[Unit 1: Einführung](
https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md?ref_type=heads#unit-1---introduction-to-gitlab)  
[Unit 2: Continuous Integration with GitLab CI](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md?ref_type=heads#unit-2---continuous-integration-with-gitlab-ci)  
[Unit 3: Continuous Deployment with GitLab & AWS](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md?ref_type=heads#unit-3---continuous-deployment-with-gitlab--aws)  
[*Unit 4 (out of scope): Deploying a dockerized application to AWS*](https://gitlab.com/gitlab-course-public/freecodecamp-gitlab-ci/-/blob/main/docs/course-notes.md?ref_type=heads#unit-4---deploying-a-dockerized-application-to-aws)  