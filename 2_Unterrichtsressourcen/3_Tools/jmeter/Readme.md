# Jmeter

[TOC]

## Introduction

Jmeter is a Tool to perform load tests. It can also be used to do some functional testing. It supports the most of common Communication Protocols (e.g. HTTP/S,SOAP,REST,LDAP,POP,IMAP,.....).

It is GUI based to develop a "Testplan" (the core unit of every jmeter tests), but can then be run in batch mode from command line.

## Installation

First download the most recent release from https://jmeter.apache.org/download_jmeter.cgi.  

Download it as tgz or zip file and extract it on your Desktop in your home directory or were ever you like it.
Install java before you start jmeter. Any openjdk from https://jdk.java.net/ will do the job or just install the jdk using apt or yum.

[Documentation of Jmeter](https://jmeter.apache.org/index.html)

## Start jmeter in GUI-Mode

I assume java can be found in your PATH. otherwise you will have to specify the JAVA_HOME directory using:
```bash
export JAVA_HOME=<whereeveryourjavahomeislocated>
```


Just start the GUI and no test:
```bash
~/apache-jmeter-5.2.1/bin/jmeter
```

If you want to open a test plan file *testplanfile.jmx* in the gui but not starting it just use:
```bash
~/apache-jmeter-5.2.1/bin/jmeter -t testplanfile.jmx
```

## Run a prepared Testplan from CLI

If you want to run a test plan file *testplanfile.jmx* in the CLI without a GUI:
```
~/apache-jmeter-5.2.1/bin/jmeter -nt testplanfile.jmx
```
The output will look like:
```
Created the tree successfully using testplanfile.jmx
Starting standalone test @ 2024 Nov 26 11:23:25 CET (1732616605360)
Waiting for possible Shutdown/StopTestNow/HeapDump/ThreadDump message on port 4445
summary +      2 in 00:00:06 =    0.3/s Avg:  1373 Min:   612 Max:  2134 Err:     0 (0.00%) Active: 1 Started: 1 Finished: 0
summary =      2 in 00:00:06 =    0.3/s Avg:  1373 Min:   612 Max:  2134 Err:     0 (0.00%)
Tidying up ...    @ 2024 Nov 26 11:23:31 CET (1732616611471)
... end of run
```

## Create a Testplan

Testplan Elements are arranged in a hierarchical. This means, that the most elements should be placed on the right level of the Testplan-Hierarchy. Basic Elements of every testplan are:

| Name of Element | What is configured here | What is typically a subelement |
| - | - | - |
| Testplan | User Defined Variables, such to use, maybe other centrally as usernames and hostnames ma aged Variables |Thread Group, Config Elements (e.g. Login and Connection Information for HTTP,...) |
| Thread Group | How many threads should be started,how fast should be the ramp up of the number of threads. How many times should the threads be restarted when they finish. | Logic Controllers (such as loops), Config Elements (e.g. Login and Connection Information for HTTP, LDAP,....),  Samplers (eg. Ldap bind Requests which should be run only once per Thread.) |
| Loop Controller | How many times all the request under the loop should be run. | Samplers (e.g. LDAP-Searches, HTTP Requests), Timers (e.g. Random Timers to delay the loops a bit in a random fashion), Config Elements (e.g. CSV Data Set Config input sources for the loop) |
| Samplers | How the request should be performed (e.g. excact LDAP Search request with basedn,....) | Assertions (Response Assertion e.g. To parse the output for results expected) |
| Assertions | The check for the result of a sampler is defininded here | |
| CSV Data Set Config | CSV Files to read from and fill variables for the loop.  They should be configured in to Loop Controller, in order to get different values for variables in each cycle | | 

## Sample Testplan for Gitlab

This [Testplan Gitlab.jmx](https://gitlab.com/armindoerzbachtbz/jmeter/-/raw/main/Gitlab.jmx?ref_type=heads&inline=false) is performing some Actions on the Gitlab-Webpage.
Instructions how to run it can be found in the Repo https://gitlab.com/armindoerzbachtbz/jmeter

![Gitlab.jmx](./Gitlab_jmeter.png){width=800}
