# Selenium

[Selenium](https://www.selenium.dev/) ist ein Tool welches automatisiert einen Standard Webbrowser (Edge,Firefox,Chrome,...) bedient und so eine Webseite testet.

Eine Beispiel wie man die TBZ webseite www.tbz.ch und die Selenium Webseite www.selenium.dev testen kann ist in folgendem Repo direkt in einer Pipeline integriert. [https://gitlab.com/armindoerzbachtbz/selenium/](https://gitlab.com/armindoerzbachtbz/selenium/)
