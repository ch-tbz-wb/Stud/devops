### Code-Repository (4h)[Thanam/Patrick]
---

### Einleitung
In der modernen Softwareentwicklung sind effiziente Versionierungssysteme und strukturierte Workflows unerlässlich, um Qualität, Zusammenarbeit und die pünktliche Auslieferung von Projekten zu gewährleisten. Besonders in Projekten mit einer komplexen Codebasis und einem großen Entwicklerteam, wie dem der “Metallbau Müller GmbH”, wird die Implementierung eines geeigneten Softwareentwicklungsprozesses entscheidend. In diesem Kontext spielt das Git-Versionierungssystem eine zentrale Rolle, um die Zusammenarbeit zwischen internen Entwicklern und externen Beratern effizient zu gestalten.

---

### Themen
* Was ist Git Flow?
* Wie funktioniert Git Flow?
* Commits mit User Storys verknüpfen
* Verschiedene merge Strategien
* Pull Request erstellen und anwenden
* Branching Strategien
* Semantic Versioning
* Mono Repo vs. Multi Repo

---

### Einarbeitung / Material

* Lest die folgenden Kapitel als Einführung ins Thema
    * Version Control - https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control
    * Branching Strategien
        * Git Flow - https://www.gitkraken.com/learn/git/git-flow.
        * One Flow - https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
        * Github Flow - https://docs.github.com/de/get-started/using-github/github-flow
    * Branches in a Nutshell - https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell
    * Semantic Versioning - https://semver.org/
    * Azure DevOps Repository erstellen - https://learn.microsoft.com/en-us/azure/devops/repos/git/create-new-repo?view=azure-devops
    * git - the simple guide (Optional) - https://rogerdudler.github.io/git-guide/
    * git Reference (Optional) - https://git-scm.com/docs/gittutorial

---

### Case Study - Analyse und Implementierung eines Softwareentwicklungsprozesses für eine Metallbaufirma

#### Projektbeschreibung:
Du bist bei der Firma “Git-Innovations GmbH” als Consultant tätig. Die Firma “Metallbau Müller GmbH” plant den Ausbau der Entwicklung einer massgeschneiderten Software zur Optimierung ihrer komplexen Fertigungsprozesse. Du wurdest als externer Consultant zur Unterstützung beigezogen. Das Projektteam besteht aus 50 internen Entwicklern. Bis jetzt haben die internen Entwickler lokal und ohne Versionskontrolle gearbeitet. Als externer Dienstleister sollst du die Implementierung und Optimierung des Softwareentwicklungsprozesses begleiten, insbesondere durch die Einführung und Anwendung moderner Git-Workflows und -Praktiken.

#### Aufgabenstellung:
Als externer Dienstleister sollt ihr eine umfassende Untersuchung und Implementierung des Git-Workflows für das Projektteam durchführen. Die Ergebnisse sollen im Plenum besprochen werden.

#### Auftrag:

##### Teil 1 - Methodik: [2L]
1. Lest die relevanten Informationen aus dem Material.
2. Überlegt euch welche Branching Strategie für die Metallbau Müller GmbH geeignet sein könnte.
3. Macht euch Gedanken zu folgenden Punkten und begründet eure Entscheidungen wo nötig:
    * Welche Versionierungsstrategie ist in welchen Fällen sinnvoll?
    * Schlagt Ihr der Firma Metallbau Müllber GmbH ein Single oder Multi-Repo Ansatz vor?    
    * Welche Tools sollen die Entwickler nutzen um gemeinsam in der Softwareentwicklung arbeiten zu können?
4. Git Übung - https://learngitbranching.js.org/


##### Teil 2 - Übung: [2L]
Empfehlung: Schreibe in der Console die Git-Befehle und dokumentiere sie.
1. Erstellt ein neues Azure DevOps Repository und klone den main Branch auf dein lokales Gerät.
2. Erstelle die notwendigen Branches gemäss der Git Flow Branching Strategie in Azure DevOps.
3. Wechsle lokal zum Develop Branch.
4. Füge die Datei [index.html](../4_Material/GitFlows/index.html) zum Develop Branch hinzu und pushe die Änderungen auf das Azure DevOps Online Repository.
5. Erstelle einen neuen Pull Request gemäss [Azure Pullrequest](https://learn.microsoft.com/en-us/azure/devops/repos/git/pull-requests?view=azure-devops&tabs=browser#create-a-pr-from-a-pushed-branch), welcher die Änderungen von develop -> main beinhaltet.
6. Erstelle einen neuen Feature Branch und füge einige Änderungen zu der index.html Datei hinzu.
7. Merge den Feature Branch in den Develop Branch.
8. Challenge: Versuche jetzt mit den beiden Branches einen Merge-Konflikt zu erzwingen.
9. Challenge: Löse den Merge-Konflikt in deinem Ziel-Branch.

Weitere Übungen:
* Erweitertes Branching / Branch Management - https://git-scm.com/book/en/v2/Git-Branching-Branch-Management
* Rebasing - https://git-scm.com/book/en/v2/Git-Branching-Rebasing


---

### Zusätzliches Material
Here are the formatted links:

* **Was ist Git Flow? & Wie funktioniert Git Flow?**
    * [Atlassian Bitbucket: "Gitflow Workflow"](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)
    * [Vincent Driessen's Original Blog Post: "A successful Git branching model"](https://nvie.com/posts/a-successful-git-branching-model/)

* **Commits mit User Storys verknüpfen**
    * [Atlassian: "Link JIRA issues to Git commits"](https://confluence.atlassian.com/jirasoftwarecloud/link-jira-issues-to-git-commits-947024402.html)
    * [GitLab Docs: "User Story Mapping"](https://docs.gitlab.com/ee/user/project/issue_board/user_story_mapping.html)

* **Verschiedene merge Strategien**
    * [Git Documentation: "Git Merge Strategies"](https://git-scm.com/docs/merge-strategies)
    * [Atlassian: "Merging vs. Rebasing"](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)
    * [Übersicht Merging Strategien](https://lukemerrett.com/different-merge-types-in-git/)

* **Pull Request erstellen und anwenden**
    * [GitHub Docs: "About pull requests"](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/about-pull-requests)
    * [GitLab Docs: "Merge requests"](https://docs.gitlab.com/ee/user/project/merge_requests/)

* **Branching Strategien**
    * [Microsoft DevOps: "Adopt a Git branching strategy"](https://docs.microsoft.com/en-us/azure/devops/repos/git/git-branching-guidance)
    * [Atlassian: "Git Branching Strategies"](https://www.atlassian.com/git/tutorials/comparing-workflows)

* **Semantic Versioning**
    * [Semantic Versioning 2.0.0 Specification](https://semver.org/)
    * [npm Documentation: "About semantic versioning"](https://docs.npmjs.com/about-semantic-versioning)

* **Mono Repo vs. Multi Repo**
    * [Atlassian: "Monorepos: What they are and how to get started"](https://www.atlassian.com/git/tutorials/monorepos)
    * [Thoughtworks: "Monorepo vs Polyrepo"](https://www.thoughtworks.com/insights/blog/monorepo-vs-polyrepo)
