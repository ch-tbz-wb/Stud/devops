# DevOps study case techwave solutions 
## Ist-Situation der Firma TechWave Solutions

TechWave Solutions ist ein mittelgroßes Softwareentwicklungsunternehmen, das vor zehn Jahren gegründet wurde und sich auf maßgeschneiderte Softwarelösungen für verschiedene Branchen spezialisiert hat. Trotz ihres guten Rufs für qualitativ hochwertige Produkte steht die Firma vor mehreren Herausforderungen, die durch die traditionelle Projektorganisation und den klassischen Softwareentwicklungszyklus (SDLC) verursacht werden.

Derzeit verwendet TechWave Solutions einen traditionellen SDLC-Ansatz, der die Phasen Planung, Design, Entwicklung, Testing, Deployment und Wartung umfasst. In der Planungsphase werden die Anforderungen definiert und ein Projektplan erstellt, wobei die Kommunikation hauptsächlich zwischen dem Business-Team und den Projektmanagern erfolgt. Anschließend erstellt das Architekturteam das Design der Software, das dann an die Entwicklungsteams weitergegeben wird. Nach der Entwicklungsphase wird die Software an das Qualitätssicherungsteam (QA) übergeben, das umfangreiche Tests durchführt. Schließlich übernimmt das Betriebsteam die Software für das Deployment und die Wartung, wobei es die Software im laufenden Betrieb überwacht und auftretende Probleme behebt.

TechWave Solutions steht jedoch vor mehreren Herausforderungen, die durch diesen Ansatz verursacht werden. Die langen Entwicklungszyklen führen dazu, dass neue Funktionen nur langsam auf den Markt kommen. Die Kommunikation zwischen den verschiedenen Teams – Business, Architektur, Entwicklung, QA und Betrieb – ist oft ineffizient, was zu Informationsverlusten und Missverständnissen führt. Die Übergabe von Projekten zwischen den Teams verläuft nicht reibungslos, insbesondere die Übergabe von der Entwicklung an das QA-Team und vom QA-Team an das Betriebsteam, was häufig zu Problemen und Verzögerungen führt.

Zusätzlich erschwert die langsame Reaktionszeit auf Kundenfeedback das schnelle Umsetzen von Änderungen. Viele Prozesse, besonders im Bereich Testing und Deployment, sind manuell und anfällig für Fehler, was zu einer erhöhten Arbeitsbelastung und zusätzlichen Verzögerungen führt. Die Automatisierung in den Bereichen Continuous Integration (CI) und Continuous Deployment (CD) ist kaum vorhanden, was die Effizienz in der Entwicklung und im Betrieb beeinträchtigt. Zudem arbeiten die Teams in Silos, was zu einer isolierten Arbeitsweise führt. Jedes Team konzentriert sich auf seine eigenen Aufgaben, ohne die Abhängigkeiten und Zusammenhänge mit den anderen Teams zu berücksichtigen.

Insgesamt führen diese Herausforderungen bei TechWave Solutions zu ineffizienten Prozessen, langsamer Markteinführung neuer Funktionen und einer insgesamt reduzierten Wettbewerbsfähigkeit.


## Study Case Übungsauftrag: Übergang von klassischer Projektorganisation zu DevOps bei TechWave Solutions

### Ziel:
TechWave Solutions möchte die Effizienz und Reaktionsfähigkeit ihrer Softwareentwicklung und -bereitstellung verbessern, indem sie von einer traditionellen Projektorganisation zu einem DevOps-Ansatz übergehen. Die folgenden Aufgaben und Fragen sollen helfen, die Herausforderungen zu identifizieren und den Übergang zu DevOps zu gestalten.

### Aufgabenstellung:

**1. Analyse der aktuellen Herausforderungen:**
   - Welche spezifischen Probleme treten bei den langen Entwicklungszyklen auf?
   - Wie beeinträchtigen ineffiziente Kommunikationswege zwischen den Teams die Projektabwicklung?
   - Welche Schwierigkeiten entstehen bei den Übergaben zwischen den Teams, insbesondere zwischen Entwicklung, QA und Betrieb?
   - Wie wirkt sich die langsame Reaktionszeit auf Kundenfeedback auf die Kundenzufriedenheit aus?
   - In welchen Bereichen führen manuelle Prozesse zu Fehlern und Verzögerungen?

**2. Untersuchung der aktuellen Prozesse:**
   - Welche Phasen des traditionellen SDLC werden derzeit bei TechWave Solutions verwendet?
   - Wie erfolgt die Anforderungsaufnahme und -planung?
   - Wie wird das Softwaredesign erstellt und an die Entwicklungsteams übergeben?
   - Wie verläuft der Testprozess im QA-Team?
   - Welche Schritte sind beim Deployment und in der Wartung involviert?

**3. Bewertung der aktuellen Automatisierungspraktiken:**
   - Welche Bereiche des Entwicklungs- und Bereitstellungsprozesses sind derzeit automatisiert?
   - Welche manuellen Schritte existieren und warum sind sie problematisch?
   - Gibt es bereits Ansätze oder Überlegungen zu Continuous Integration (CI) und Continuous Deployment (CD)?

**4. Identifikation von Silos und Kommunikationsproblemen:**
   - Wie arbeiten die verschiedenen Teams (Business, Architektur, Entwicklung, QA, Betrieb) aktuell zusammen?
   - Welche Kommunikationsmethoden und -werkzeuge werden verwendet?
   - Wo treten die häufigsten Missverständnisse und Informationsverluste auf?

**5. Erforschung von möglichen Lösungen:**
   - Welche Maßnahmen könnten die Entwicklungszyklen verkürzen und den Übergang von der Entwicklung zum Betrieb beschleunigen?
   - Wie könnten die Kommunikationswege zwischen den Teams verbessert werden?
   - Welche Automatisierungslösungen könnten eingeführt werden, um Fehler zu reduzieren und die Effizienz zu steigern?
   - Welche Strategien könnten helfen, die Silos zwischen den Teams aufzulösen und eine kollaborativere Arbeitsweise zu fördern?

**6. Planung des Übergangs zu DevOps:**
   - Welche Schritte sind erforderlich, um von der traditionellen Projektorganisation zu einem DevOps-Ansatz überzugehen?
   - Welche Änderungen in der Organisationsstruktur und Unternehmenskultur sind notwendig?
   - Wie können Teams auf den Übergang vorbereitet und geschult werden?
   - Welche Tools und Technologien sind für die Umsetzung von DevOps erforderlich?

**Abschließende Fragen:**
   - Wie kann sichergestellt werden, dass der Übergang zu DevOps kontinuierlich verbessert wird?
   - Welche Metriken und KPIs sollten verwendet werden, um den Erfolg des DevOps-Ansatzes zu messen?
   - Wie kann Feedback von Kunden und internen Teams effektiv in den neuen DevOps-Prozess integriert werden?

---
**Quellen:**
- https://www.splunk.com/de_de/data-insider/software-development-lifecycle.html
- https://aws.amazon.com/de/devops/what-is-devops/
- https://medium.com/@nalawade1000work/sdlc-vs-devops-25ff2dd0accf