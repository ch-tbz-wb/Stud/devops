# 4 types of work

## Arbeitsgruppenauftrag: "Die vier Arten von Arbeitstypen in DevOps"

### Einleitung

Im Rahmen dieses Projekts werden wir die vier Arten von Arbeitstypen in DevOps anhand des Buches "The Phoenix Project" untersuchen. Euer Auftrag besteht darin, euch in Arbeitsgruppen zusammenzufinden und zu jeder Art von Arbeitstyp diverse Fragen zu beantworten. Dies hilft euch, die verschiedenen Aspekte des DevOps-Ansatzes besser zu verstehen und wie diese in der Praxis angewendet werden können.

#### Arbeitsgruppen und Aufgabenstellung 

1. **Arbeitsgruppenbildung**
   - Bildet Gruppen von 3-4 Personen.
   - Jede Gruppe erhält eine der vier Arten von Arbeitstypen zur Bearbeitung.

2. **Einlesen in das Thema** [1L]
   - Lest das Kapitel über die vier Arten von Arbeitstypen in "The Phoenix Project".
     - https://www.e4developer.com/2018/03/24/the-phoenix-project-a-key-to-understanding-devops/
     - https://clickup.com/blog/the-phoenix-project-summary/?utm_source=google-pmax&utm_medium=cpc&utm_campaign=gpm_cpc_ar_nnc_pro_trial_all-devices_tcpa_lp_x_all-departments_x_pmax&utm_content=&utm_creative=_____&gad_source=1&gclid=CjwKCAjwkJm0BhBxEiwAwT1AXB_AVSH4dz4Uzc0dL1kaQLZZKGyC6Nooh2RJUtxIPxVTKbgU7jmoDBoCDOcQAvD_BwE
     - https://resources.scrumalliance.org/Article/three-pillars-of-scrum
   - Sucht nach zusätzlichen Ressourcen und Artikeln online, die das Thema erläutern.

3. **Fragen zur Bearbeitung**
   - Jede Gruppe soll die folgenden Fragen zu ihrem zugewiesenen Arbeitstyp beantworten und die Antworten in einem schriftlichen Bericht zusammenfassen.

#### Fragen zu den vier Arten von Arbeitstypen [2L]

1. **Business Projects (Geschäftsprojekte)**
   - Was sind Business Projects und warum sind sie wichtig?
   - Gebt zwei konkrete Beispiele für Business Projects.
   - Welche Tools und Methoden werden verwendet, um Business Projects zu verwalten?
   - Wie beeinflussen Business Projects die Unternehmensziele und -strategien?
   - Wie siehts bei euch in der Praxis aus? Habt ihr Business Projects? Wie werden diese umgesetzt?

2. **Internal IT Projects (Interne IT-Projekte)**
   - Was sind Internal IT Projects und warum sind sie notwendig?
   - Gebt zwei konkrete Beispiele für Internal IT Projects.
   - Welche Herausforderungen gibt es bei der Verwaltung von Internal IT Projects?
   - Welche Best Practices und Tools unterstützen die Durchführung dieser Projekte?
   - Wie siehts bei euch in der Praxis aus? Habt ihr Internal IT Projects? Wie werden diese umgesetzt?

3. **Changes (Änderungen)**
   - Was versteht man unter Changes in der IT?
   - Warum sind regelmäßige Changes wichtig für die Systemstabilität?
   - Gebt zwei Beispiele für typische Changes, die in einem Unternehmen vorkommen.
   - Wie werden Changes typischerweise verwaltet und dokumentiert?
   - Wie siehts bei euch in der Praxis aus? Wie werden Changes bei euch umgesetzt?

4. **Unplanned Work (Ungeplante Arbeit)**
   - Was ist Unplanned Work und wie entsteht sie?
   - Welche Auswirkungen kann Unplanned Work auf den Geschäftsbetrieb haben?
   - Gebt zwei Beispiele für Unplanned Work in einem IT-Kontext.
   - Welche Strategien gibt es, um Unplanned Work zu minimieren und zu bewältigen?
   - Wie siehts bei euch in der Praxis aus? Wie wird Unplanned Work bei euch gehandhabt?

#### Präsentation der Ergebnisse [1L]

- Jede Gruppe bereitet eine kurze Präsentation (max. 10 Minuten) vor, in der die Ergebnisse vorgestellt werden.
- Achtet darauf, dass eure Präsentation gut strukturiert und verständlich ist. Nutzt visuelle Hilfsmittel wie Diagramme und Präsentationsfolien.

#### Hilfsmittel und Ressourcen

- **Buch**: "The Phoenix Project: A Novel About IT, DevOps, and Helping Your Business Win" von Gene Kim, Kevin Behr und George Spafford.
- **Online-Ressourcen**:
  - [DevOps Handbook](https://devops.com/handbook/)
  - Artikel und Fallstudien über DevOps-Praktiken (z.B. Atlassian, AWS, Red Hat)