# Was ist CALMS?

## Arbeitsgruppenauftrag: "CALMS im DevOps-Kontext"

### Einleitung

Diese Aufgabe umfasst die fünf Aspekte des CALMS-Modells. Ziel ist es, CALMS im DevOps-Kontext anhand des Buches “DevOps For Dummies” zu untersuchen. Euer Auftrag besteht darin, euch in Arbeitsgruppen zusammenzufinden und zu jedem Aspekt des CALMS-Modells diverse Fragen zu beantworten. Dies hilft euch, die verschiedenen Dimensionen des DevOps-Ansatzes besser zu verstehen und wie diese in der Praxis angewendet werden können.

#### Arbeitsgruppen und Aufgabenstellung 

1. **Arbeitsgruppenbildung**
   - Bildet Gruppen von 3-4 Personen.
   - Jede Gruppe erhält eine der füf Themen Culture, Automation, Lean, Measurement und Sharing zur Bearbeitung.

2. **Einlesen in das Thema** [2L]
   - Buch Devops for dummies
      - Culture
         - Kapitel 1 - Introducing DevOps
      Automation
         - Kapitel 6 - Embracing the New Development Life Cycle
         - Kapitel 10 - Automating Tests Prior to Release
      - Lean 
         - Kapitel 3 - Identifying Waste
         - Kapitel 16 - Embracing Failure Successfully
      - Measurement
         - Kapuitel 5 - Measuring Your Organization
      - Sharing
         - Kapitel 13 - Creating Feedback Loops around the Customer
         - Kapitel 14 - DevOps Isn’t a Team (Except When It Is)
   - https://www.alldaydevops.com/blog/calms-a-principle-based-devops-framework
   - Sucht nach zusätzlichen Ressourcen und Artikeln online, die das Thema erläutern.

3. **Fragen zur Bearbeitung**
   - Jede Gruppe soll die folgenden Fragen entprechend Ihrem Thema in die Präsentation einfliessen lassen.

#### Fragen zu CALMS [2L]

1. **Kultur (Culture)**
   - Wie fördert Ihre Organisation eine Kultur der Zusammenarbeit und des Vertrauens innerhalb der Teams?
   - Welche Massnahmen ergreifen Sie, um Silos in Ihrer Organisation zu beseitigen?
   - Wie unterstützen Führungskräfte in Ihrem Unternehmen eine DevOps-Kultur?
   - Welche Strategien verwenden Sie, um eine kontinuierliche Verbesserung der Unternehmenskultur zu gewährleisten?

2. **Automatisierung (Automation)**
   - Welche Tools und Technologien verwenden Sie zur Automatisierung der Build-, Test- und Deploy-Prozesse?
   - Wie integrieren Sie Infrastructure as Code (IaC) in Ihre Automatisierungsstrategien?
   - Welche Vorteile haben Sie durch die Automatisierung wiederholbarer Aufgaben festgestellt?
   - Wie stellen Sie sicher, dass Ihre Automatisierungspipelines sicher und fehlerfrei sind?

3. **Lean**
   - Wie identifizieren und beseitigen Sie Engpässe in Ihren Entwicklungsprozessen?
   - Welche Methoden verwenden Sie, um den Arbeitsaufwand sichtbar zu machen und zu minimieren?
   - Wie integrieren Sie kontinuierliche Verbesserungsprozesse in Ihre täglichen Abläufe?
   - Welche Strategien nutzen Sie, um den Wertstrom in Ihrem Unternehmen zu optimieren?

4. **Messung (Measurement)**
   - Welche KPIs (Key Performance Indicators) verwenden Sie, um den Erfolg Ihrer DevOps-Initiativen zu messen?
   - Wie sammeln und analysieren Sie Daten, um Engpässe und Verbesserungspotenziale zu identifizieren?
   - Welche Tools setzen Sie ein, um die Leistung Ihrer Systeme und Prozesse zu überwachen?
   - Wie stellen Sie sicher, dass Ihre Messmethoden zuverlässig und aussagekräftig sind?

5. **Teilen (Sharing)**
   - Welche Plattformen und Tools nutzen Sie, um Wissen und Informationen innerhalb der Teams zu teilen?
   - Wie fördern Sie den Austausch von Best Practices und Lessons Learned zwischen verschiedenen Abteilungen?
   - Welche Massnahmen ergreifen Sie, um eine offene Kommunikationskultur zu etablieren?
   - Wie integrieren Sie Feedback-Schleifen in Ihre Arbeitsprozesse, um kontinuierliche Verbesserungen zu unterstützen?

#### Präsentation der Ergebnisse [1L]

- Jede Gruppe bereitet eine kurze Präsentation (ca. 10 Minuten) vor, in der die Ergebnisse vorgestellt werden.
- Achtet darauf, dass eure Präsentation gut strukturiert und verständlich ist. Nutzt visuelle Hilfsmittel wie Diagramme und Präsentationsfolien.

#### Hilfsmittel und Ressourcen

- **Buch**: "DevOps for dummies" von Sanjeev Sharma und Bernie Coyne.
- **Online-Ressourcen**:
  - [DevOps Handbook](https://devops.com/handbook/)
  - Artikel und Fallstudien zu CALMS