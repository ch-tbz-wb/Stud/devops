### Artefakte [Thanam/Patrick]
---

### Einleitung
In dieser Aufgabe geht es um das Erstellen und Hochladen von Artefakten mit Hilfe einer Azure DevOps Pipeline. Ziel ist es, den Build-Prozess einer .NET-Anwendung zu automatisieren und die generierten Artefakte in Azure Artifacts zu speichern. Dies ermöglicht eine strukturierte Verwaltung und Wiederverwendung der Artefakte in weiteren Entwicklungs- und Deployment-Schritten.

---

### Themen
* Artefakt Upload mit einer Azure DevOps Pipeline.

---

### Einarbeitung / Material

* Azure Pipeline für .net Applikationen
    * https://learn.microsoft.com/en-us/azure/devops/pipelines/ecosystems/dotnet-core?view=azure-devops&tabs=yaml-editor
* Azure Artifacts
    * https://learn.microsoft.com/en-us/azure/devops/artifacts/start-using-azure-artifacts?view=azure-devops&tabs=nuget%2Cnugetserver
    * https://learn.microsoft.com/en-us/azure/devops/artifacts/get-started-artifacts-ai?view=azure-devops&tabs=withoutgithubcopilot
    * https://learn.microsoft.com/en-us/azure/devops/artifacts/concepts/best-practices?view=azure-devops
    * https://learn.microsoft.com/en-us/azure/devops/pipelines/artifacts/pipeline-artifacts?view=azure-devops&tabs=yaml
    * https://learn.microsoft.com/en-us/azure/devops/pipelines/tasks/reference/download-pipeline-artifact-v2?view=azure-pipelines
* Generell Pipeline
    * https://learn.microsoft.com/en-us/azure/devops/pipelines/build/variables?view=azure-devops&tabs=yaml

---
### Übersicht

<img src="../4_Material/Diagramme/Artifacts.drawio.png" alt="Themen" width="700" align="center">

### Aufgabe

1. Erstellt ein neues Azure DevOps Projekt mit dem Namen Artifacts.  
2. Importiere das Repository in das neu erstellte Projekt.
    * https://github.com/Azure-Samples/app-service-web-dotnet-get-started
3. Erstellt eine neue Pipeline mit dem Namen "Artifact-Producer" welche die folgenden Tasks ausführt:
    * Build des .net Projektes (VSBuild@1).
    * Veröffentlicht das gebaute Artefakt als Pipeline Artefakt mit dem Namen "Artifactpackage".
    * Die Pipeline soll manuell getriggert werden.

4. Erstelle einen neuen Azure Artifacts Feed mit dem Namen "artifactsfeed".  
<img src="../4_Material/Bilder/Artifact_feed.png" alt="Themen" width="500" align="center">


5. Setze die Rechte für den Build Service, damit dieser Artefakte hochladen kann.  
<img src="../4_Material/Bilder/Artifact-permissions.png" alt="Themen" width="500" align="center">  

6. Erstelle eine zweite Pipeline mit dem Namen "Artifact-Consumer" welche folgende Aufgaben wahrnimmt:
    * Herunterladen der in "Artifact-Producer" veröffentlichten Artefakte.
    * Die Pipeline soll manuell getriggert werden.
    * Optional (Challange) - Die Pipeline soll nach der "Artifact-Producer" Pipeline automatisch getriggert werden.
    * Veröffentlichen des Artifacts auf Azure Artifacts.
        * Verwende den Namen "aspnet-get-started" für das Artefakt welches du veröffentlichst.