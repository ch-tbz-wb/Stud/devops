
# Was sind "The 3 Ways"? (3h)
[TOC]
## Als Einstieg :
Lest folgenden Artikel (10 min)

https://itrevolution.com/articles/the-three-ways-principles-underpinning-devops/

## Auftrag 1
Und beantwortet folgende Fragen in 2er-3er Gruppen (20 min):
- The first way: 
  - Was würdet ihr machen wenn ihr in einem Projekt nur ein kleines Puzzle-Teil des ganzen implementieren müsst?
  - Warum ist das Gesamtsystem wichtig?
- The second way:
  - Warum solltet ihr möglichst schnell Feedbacks kriegen?
  - Wie könnt ihr das organisieren?
- The third way:
  -  Warum muss man ein paar Risiken in Kauf nehmen?
  -  Wie hilft dies beim Vorwärtskommen?



## Einführungsbeispiel von mir

Ein kleines Projekt bei dem ich vor kurzem beteiligt war, war die Migration von einer grösseren Mailplattform (2 Mio Kunden) auf eine komplett neue Software und Infrastrukturumgebung.
Dabei hatte ich einen kleinen Anteil zu leisten. Es gibt Log-File-Analysen um Spammer, Hacker und andere Anomalien zu finden. Diese mussten auf die neue Plattform migriert und angepasst werden.

- Projekt Abuse Searches **the first way**
  -  Das Projektziel ist der Go-Live -> ohne dass Abuse funktioniert, kein Go-Live -> D.h. die ganze Kette muss funktionieren und nicht nur mein Teil (Ein paar Splunk searches) 
  - Es gibt verschiedene Stakeholder 
    - Entwickler von Abuse Backend (Schnittstelle um Sperrungen auszulösen) 
    - Operation (es muss weiterhin zuverlässig gesperrt werden, damit keine Picket-Einsätze nötig sind)
    -  Business will Go-Live ankündigen
  -  D.h. bis da hin muss nach Pareto-Prinzip (80:20 Regel) mit möglichst kleinem Aufwand möglichst viel migriert und angepasst werden.
- Projekt Abuse Searches **the second way** 
  - Um die Integration mit dem Abuse-Backend zu laufen zu kriegen, muss ich möglichst schnell feedback kriegen.
    - Schnittstellen zum Backend erstellen und testen, bevor ich überhaupt meine Arbeit mit den Searches beginne (kurze Feedback Loops durch online Zusammenarbeit mit Entwicklern von Abuse Backend)
    - Erste Searches migrieren und immer Feedback über die ganze Abusekette einholen (Know the people involved (Die ganze Abusekette + Splunk Administratoren + Business habe ich im Boot halten müssen. Dabei gibt es immer wieder Abteilungsgrenzen die man überschreiten muss um Feedback zu geben und Feedback abzuholen))
- Projet Abuse Searches **the third way**
  - Am Anfang geht es darum sich einzuarbeiten. Dabei muss man sich trauen Fehler zu machen, damit die Lernkurve steil ist. 
  - Es müssen Risiken in Kauf genommen werden, die tragbar sind. 
    - z.B. musste ich Berechtigungen umstellen, bei denen mir nicht 100% klar war, was die Konsequenzen sind. Grösster Ausfall: Es kann fuer 1.5 Stunden nichts gesperrt werden -> Es gibt ein paar Spammer die 1.5 h Freude haben und ein paar 1000 Spam-Mails die zusätzlich in die ganze welt geschickt werden. Ist das ein Problem? Reisst mir jemand wegen dem den Kopf ab? Ich habe entschieden nein und meine Kollegen informiert. Take the Risk. Haetten sie anders entschieden. Ja, vielleicht, aber dann hätten wir einen riesen Zirkus machen müssen um weiterzukommen.

### Zusammenfassend
- Ziele konnten erreicht werden in der richtigen Zeit, was gar nicht so selbstverständlich war. Aber es gab auch technische Schulden die dadurch aufgebaut wurden.
- Es gab sogar eine Pipeline in diesem Setup die das deployment von Splunk-Apps machte (nicht von uns verwaltet sondern vom Splunk Team, aber wir konnten einfach im Git neue sourcen mergen -> Apps wurden dann deployed und wir hatten ein Feedback innerhalb von Minuten anstatt Tagen) 
- Es gab bei den Entwicklern des Abuse-Backends eine Pipeline die die App Deployed.
- Es gab eine Rest-API um "Abuse" Searches auf dem Backend zu konfigurieren (noch manueller Prozess), könnte aber auch automatisiert werden. (Technische Schuld)
- In einen ersten Schritt wurden alle bestehenden Searches möglichst unangetastet migriert (Zeitdruck). Dabei wurden technische Schulden sichtbar (mehrere Suchen machen dasselbe, mehrere Codesnippets waren redundant, ....). Diese technischen schulden wurden in einen zweiten Schritt abgebaut und so dokumentiert, dass sie nicht wieder auftreten sollten.
- Am Schluss hatten alle die daran gearbeitet haben ein gutes Gefühl (Kultur ändert sich).

## Auftrag 2
Überlegt euch zu jedem der 3 Wege was dabei 
- kulturell (d.h. heisst in der Art wie zusammengearbeitet wird) getan werden muss
- technisch getan werden kann um dies zu erleichtern.

Macht folgende Tabelle


| The .. Way         	| kulturelle Herausforderungen bzw. Lösungen  | technisch Herausforderungen bzw. Lösungen | 
|------------------	|------------ |- |
| The first way   	|           	|
| The second way          	|       	|
| The third way           	|        	|



## Auftrag 3
Überlegt euch für jeden der 3 Wege ein Beispiel aus eurem Betrieb, in dem das z.T. umgesetzt wird und was man noch besser machen könnte.
Dabei muss es nicht unbedingt nötig sein neue Tools einzuführen, sondern gewisse "Probleme" lassen sich besser organisatorisch lösen.



## Links 
https://agile-coach-academy.de/das-dritte-prinzip-von-devops/
https://fourweekmba.com/de/Das-Phoenix-Projekt/

