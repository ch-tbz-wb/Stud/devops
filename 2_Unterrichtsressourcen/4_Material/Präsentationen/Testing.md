# Testing Prinzipien

## Shift-Left-Ansatz

Der **Shift-Left-Testing-Ansatz** ist eine Testing-Strategie im **Softwareentwicklungsprozess**, bei der Testaktivitäten so früh wie möglich in den Entwicklungszyklus verlagert werden. Der Begriff „Shift Left“ bedeutet, dass das Testen von der traditionellen späten Phase (rechts im typischen Wasserfallmodell) **nach links** in den Entwicklungsprozess verschoben wird. 

Genaueres findet ihr unter [Grundidee des Shift-Left-Ansatzes](./shiftleftansatz.md)

## Static Testing vs. Dynamik Testing

- **Static Testing** konzentriert sich auf die Verhinderung von Fehlern durch Analyse, ohne die Software auszuführen. Es ist präventiv und kosteneffizient.  
- **Dynamic Testing** prüft das Verhalten der Software zur Laufzeit und stellt sicher, dass sie in realen Szenarien korrekt funktioniert.  

Beide Ansätze sind komplementär und sollten in einem umfassenden Testprozess kombiniert werden, um sowohl frühzeitig Fehler zu erkennen als auch sicherzustellen, dass die Software den Anforderungen entspricht.

Genaueres findet ihr unter [Static vs. Dynamic Testing](./static_vs_dyniamic.md)

Im Umfeld von Security Testing spricht man auch von **SAST**(Static Application Security Testing) und **DAST**(Dynamic Application Security Testing).

# Testing-Methoden-Overview

Es gibt eine Ganze Menge Testmethoden. Ich habe versucht diese graphisch in einem Diagramm darzustellen, mit Überlappungen.

| |
|-|
| ![](../Diagramme/Testing_Overview-Automatisierte%20Tests.drawio.png)|
| ![](../Diagramme/Testing_Overview-Manuelle%20Tests.drawio.png)|


Source: ChatGPT

Hier ist eine Übersicht der verschiedenen Testing-Methoden im DevOps-Lifecycle als Tabelle, zusammen mit den gängigen Tools für jede Phase. Diese Übersicht zeigt, dass es oft fließende Übergänge zwischen den Testtypen gibt, was zur Flexibilität des DevOps-Ansatzes passt.

| **Testtyp**                    | **Beschreibung**                                                                                                                                           | **Tools**                                                                                                                                                                     | **Abgrenzung / Besonderheit**                                                       |
|--------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|
| **Unit Testing**               | Testet einzelne Komponenten oder Funktionen isoliert, um sicherzustellen, dass sie wie erwartet arbeiten.                                                  | JUnit, NUnit, PyTest, Mocha, Jest                                                                                                                                            | Oftmals erste Teststufe im DevOps-Lifecycle, stark isoliert von anderen Komponenten.|
| **Integration Testing**        | Überprüft, ob verschiedene Komponenten zusammenarbeiten und miteinander kommunizieren können.                                                              | Postman, SoapUI, JUnit, PyTest, Selenium                                                                                                                                    | Übergang zu Systemtests, deckt oft auch API-Kommunikation ab.                       |
| **API Testing**                | Testet die Funktionalität, Zuverlässigkeit und Sicherheit von APIs, die zwischen Komponenten kommunizieren.                                                | Postman, SoapUI, REST Assured, Katalon, Newman                                                                                                                              | Überschneidet sich oft mit Integration und Functional Testing.                       |
| **Functional Testing**         | Validiert, dass ein System oder eine Anwendung die funktionalen Anforderungen gemäß Spezifikationen erfüllt.                                               | Selenium, TestComplete, Cucumber, Appium                                                                                                                                    | Kann APIs, Benutzeroberflächen oder Kernfunktionen abdecken, fließender Übergang.   |
| **End-to-End Testing (E2E)**   | Testet komplette Geschäftsprozesse über mehrere Systeme hinweg, um reale Benutzerinteraktionen zu simulieren.                                              | Selenium, Cypress, Playwright, TestCafe                                                                                                                                      | Überlappt oft mit Functional Testing und kann auch Performance-Tests umfassen.       |
| **Regression Testing**         | Vergewissert sich, dass neue Änderungen keine bestehenden Funktionen beeinträchtigen.                                                                     | Selenium, Cypress, Jenkins (zur Automatisierung), Robot Framework                                                                                                           | Überlappt häufig mit automatisierten Tests in anderen Bereichen.                    |
| **Performance Testing**        | Testet, ob die Anwendung unter bestimmten Lastbedingungen funktioniert und welche Grenzen sie hat.                                                         | JMeter, Gatling, Locust, LoadRunner                                                                                                                                         | Kann auch als Stresstest oder Lasttest kategorisiert werden, Schnittpunkt mit anderen Tests. |
| **Security Testing**           | Bewertet die Sicherheit der Anwendung, um Schwachstellen und Bedrohungen zu identifizieren.                                                                | OWASP ZAP, Burp Suite, Nessus, Checkmarx, SonarQube                                                                                                                         | Integration in DevSecOps-Pipelines zur kontinuierlichen Sicherheitsüberwachung.     |
| **Usability Testing**          | Bewertet die Benutzerfreundlichkeit und Effizienz der Benutzeroberfläche und User Experience (UX).                                                        | UserTesting, Lookback, Maze, Hotjar                                                                                                                                          | Oft nicht automatisiert und auf Benutzerfeedback angewiesen.                        |
| **Acceptance Testing**         | Testet, ob die Anwendung den Anforderungen des Kunden entspricht (User Acceptance Test, UAT).                                                             | FitNesse, Cucumber, SpecFlow, Selenium                                                                                                                                       | Letzte Stufe vor der Produktivsetzung, kann sowohl manuell als auch automatisiert sein. |
| **Smoke Testing**              | Schnelltest zur Überprüfung der wichtigsten Funktionen, um sicherzustellen, dass das System bereit für umfangreichere Tests ist.                           | Selenium, Cypress, TestNG, Postman                                                                                                                                            | Auch als "Sanity Check" bekannt, Teil der CI/CD-Pipeline zum schnellen Feedback.    |
| **Exploratory Testing**        | Tester erkunden das System spontan, um potenzielle Fehler zu finden. Keine festen Testfälle, sondern exploratives Vorgehen.                                | JIRA (zur Dokumentation), TestRail, PractiTest                                                                                                                               | Selten automatisiert, hängt stark vom Wissen und der Kreativität des Testers ab.    |
| **Chaos Engineering / Testing**| Simuliert unerwartete Ereignisse, um die Widerstandsfähigkeit des Systems zu testen.                                                                      | Chaos Monkey, Gremlin, Litmus                                                                                                                                               | Wird oft im laufenden Betrieb oder in Staging-Umgebungen angewendet.                |

### Zusammenfassung und fließende Übergänge
- Viele der genannten Tests überschneiden sich in ihren Anwendungsbereichen, und einige Tools können für mehrere Testarten genutzt werden. 
- Da DevOps den Schwerpunkt auf Continuous Testing und Feedback legt, können Tests wie Unit-, Integration- und API-Tests fließend ineinander übergehen und in frühen Stufen der CI/CD-Pipeline kontinuierlich ausgeführt werden.
- Performance, Security und Chaos Testing lassen sich ebenfalls im CI/CD-Prozess automatisieren, was zu einer weiteren Verwischung der Grenzen zwischen Testphasen führt.
  
Diese Flexibilität und die Integration verschiedener Testmethoden in die Pipeline ermöglichen es Teams, schnell und kontinuierlich auf Veränderungen zu reagieren und eine hohe Qualität zu gewährleisten.

Wie ihr ja wisst, möchte ich bei jedem Tool wissen, wie lange es schon auf dem Markt ist, ob es kostet und ob es Open-Source ist.

Hier die Tabelle, die diese Informationen enthält:

| **Tool**            | **Homepage**                                         | **Kosten**         | **Lizenzart** | **Veröffentlichungsjahr** |
|---------------------|------------------------------------------------------|---------------------|---------------|---------------------------|
| **JUnit**           | [https://junit.org](https://junit.org)               | Kostenlos          | Open Source   | 1997                      |
| **NUnit**           | [https://nunit.org](https://nunit.org)               | Kostenlos          | Open Source   | 2002                      |
| **PyTest**          | [https://docs.pytest.org](https://docs.pytest.org)   | Kostenlos          | Open Source   | 2004                      |
| **Mocha**           | [https://mochajs.org](https://mochajs.org)           | Kostenlos          | Open Source   | 2011                      |
| **Jest**            | [https://jestjs.io](https://jestjs.io)               | Kostenlos          | Open Source   | 2014                      |
| **Postman**         | [https://www.postman.com](https://www.postman.com)   | Kostenlos und kommerziell | Closed Source | 2012               |
| **SoapUI**          | [https://www.soapui.org](https://www.soapui.org)     | Kostenlos (Open Source) und kommerziell (Pro) | Open Source / Closed Source | 2005 |
| **REST Assured**    | [https://rest-assured.io](https://rest-assured.io)   | Kostenlos          | Open Source   | 2010                      |
| **Katalon**         | [https://www.katalon.com](https://www.katalon.com)   | Kostenlos und kommerziell | Closed Source | 2015               |
| **Newman**          | [https://www.npmjs.com/package/newman](https://www.npmjs.com/package/newman) | Kostenlos          | Open Source   | 2015                      |
| **Selenium**        | [https://www.selenium.dev](https://www.selenium.dev) | Kostenlos          | Open Source   | 2004                      |
| **TestComplete**    | [https://smartbear.com/product/testcomplete](https://smartbear.com/product/testcomplete) | Kommerziell | Closed Source | 1999       |
| **Cucumber**        | [https://cucumber.io](https://cucumber.io)           | Kostenlos und kommerziell | Open Source / Closed Source | 2008 |
| **Appium**          | [https://appium.io](https://appium.io)               | Kostenlos          | Open Source   | 2012                      |
| **Cypress**         | [https://www.cypress.io](https://www.cypress.io)     | Kostenlos und kommerziell | Open Source / Closed Source | 2015 |
| **Playwright**      | [https://playwright.dev](https://playwright.dev)     | Kostenlos          | Open Source   | 2020                      |
| **TestCafe**        | [https://testcafe.io](https://testcafe.io)           | Kostenlos          | Open Source   | 2011                      |
| **JMeter**          | [https://jmeter.apache.org](https://jmeter.apache.org) | Kostenlos       | Open Source   | 1998                      |
| **Gatling**         | [https://gatling.io](https://gatling.io)             | Kostenlos (Open Source) und kommerziell | Open Source / Closed Source | 2012 |
| **Locust**          | [https://locust.io](https://locust.io)               | Kostenlos          | Open Source   | 2011                      |
| **LoadRunner**      | [https://microfocus.com/loadrunner](https://microfocus.com/loadrunner) | Kommerziell | Closed Source | 1999       |
| **OWASP ZAP**       | [https://www.zaproxy.org](https://www.zaproxy.org)   | Kostenlos          | Open Source   | 2010                      |
| **Burp Suite**      | [https://portswigger.net/burp](https://portswigger.net/burp) | Kostenlos (Community) und kommerziell | Closed Source | 2004 |
| **Nessus**          | [https://www.tenable.com/products/nessus](https://www.tenable.com/products/nessus) | Kommerziell | Closed Source | 1998 |
| **Checkmarx**       | [https://checkmarx.com](https://checkmarx.com)       | Kommerziell        | Closed Source | 2006                      |
| **SonarQube**       | [https://www.sonarqube.org](https://www.sonarqube.org) | Kostenlos (Community) und kommerziell | Open Source / Closed Source | 2007 |
| **UserTesting**     | [https://www.usertesting.com](https://www.usertesting.com) | Kommerziell | Closed Source | 2007       |
| **Lookback**        | [https://lookback.io](https://lookback.io)           | Kommerziell        | Closed Source | 2015                      |
| **Maze**            | [https://maze.co](https://maze.co)                   | Kommerziell        | Closed Source | 2018                      |
| **Hotjar**          | [https://www.hotjar.com](https://www.hotjar.com)     | Kostenlos und kommerziell | Closed Source | 2014       |
| **FitNesse**        | [http://fitnesse.org](http://fitnesse.org)           | Kostenlos          | Open Source   | 2002                      |
| **SpecFlow**        | [https://specflow.org](https://specflow.org)         | Kostenlos und kommerziell | Closed Source | 2009       |
| **TestNG**          | [https://testng.org/doc](https://testng.org/doc)     | Kostenlos          | Open Source   | 2004                      |
| **Robot Framework** | [https://robotframework.org](https://robotframework.org) | Kostenlos | Open Source | 2008      |
| **JIRA**            | [https://www.atlassian.com/software/jira](https://www.atlassian.com/software/jira) | Kommerziell | Closed Source | 2002 |
| **TestRail**        | [https://www.gurock.com/testrail](https://www.gurock.com/testrail) | Kommerziell | Closed Source | 2004 |
| **PractiTest**      | [https://www.practitest.com](https://www.practitest.com) | Kommerziell | Closed Source | 2008       |
| **Chaos Monkey**    | [https://netflix.github.io/chaosmonkey](https://netflix.github.io/chaosmonkey) | Kostenlos | Open Source | 2011 |
| **Gremlin**         | [https://www.gremlin.com](https://www.gremlin.com)   | Kommerziell        | Closed Source | 2016                      |
| **Litmus**          | [https://litmus.com](https://litmus.com)             | Kommerziell        | Closed Source | 2009                      |

Diese Tabelle zeigt, die wichtigsten Details und den Einsatz in DevOps-Umgebungen zu bewerten. Viele der Tools sind schon seit Jahren im Einsatz und gelten als bewährte Lösungen.