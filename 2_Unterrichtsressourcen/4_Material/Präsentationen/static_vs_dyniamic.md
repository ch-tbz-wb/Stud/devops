# Static vs. Dynamic Testing

**Static Testing** und **Dynamic Testing** sind zwei grundlegende Ansätze im Softwaretestprozess, die sich durch ihre Methodik und den Zeitpunkt des Einsatzes im Entwicklungszyklus unterscheiden.  

---

### **Static Testing (Statisches Testen)**

**Definition**:  
Static Testing analysiert die Software, ohne sie auszuführen. Es konzentriert sich auf die Überprüfung von **Artefakten** wie Quellcode, Designs, Anforderungen oder Dokumentationen.

**Merkmale**:  
- **Keine Ausführung der Software**: Der Code oder die Dokumentation wird überprüft, ohne dass die Anwendung läuft.  
- **Frühzeitige Fehlererkennung**: Es wird oft in den frühen Phasen der Entwicklung durchgeführt (z. B. während der Anforderungsanalyse oder des Designs).  
- **Automatisiert oder manuell**: Tools können zur Unterstützung genutzt werden, aber auch manuelle Code-Reviews oder Walkthroughs sind verbreitet.

**Beispiele für Static Testing**:  
1. **Code-Review**: Entwickler prüfen den Quellcode, um Fehler oder Verstöße gegen Coding-Standards zu finden.  
2. **Anforderungsprüfung**: Überprüfung der Anforderungen auf Vollständigkeit, Widersprüche oder Missverständnisse.  
3. **Statische Codeanalyse**: Tools überprüfen automatisch den Code auf potenzielle Fehler, Sicherheitslücken oder ineffizienten Code.  
   - **Beispiele für Tools**: SonarQube, Checkmarx, Fortify., [Gitlab-SAST-Tools](https://docs.gitlab.com/ee/user/application_security/sast/index.html)

**Vorteile**:  
- Identifiziert Fehler frühzeitig (kostengünstigere Behebung).  
- Erfordert keine lauffähige Software.  
- Verbessert die Code-Qualität und hält Standards ein.

**Einschränkungen**:  
- Kann keine Laufzeitfehler oder dynamische Probleme (z. B. Speicherlecks, Integrationsfehler) finden.  
- Erfordert manuelle oder automatisierte Expertise, die zeitaufwendig sein kann.

---

### **Dynamic Testing (Dynamisches Testen)**

**Definition**:  
Dynamic Testing prüft die Software durch **Ausführung des Codes**, um ihr Verhalten und die Funktionalität unter verschiedenen Bedingungen zu bewerten.

**Merkmale**:  
- **Ausführung der Software**: Die Anwendung wird gestartet, und ihre Reaktion auf Eingaben wird beobachtet.  
- **Testet Laufzeitverhalten**: Es überprüft, ob die Software wie erwartet funktioniert, und identifiziert Laufzeitfehler.  
- **Benutzerfokus**: Es kann echte Benutzerszenarien simulieren.

**Beispiele für Dynamic Testing**:  
1. **Unit Testing**: Test einzelner Code-Einheiten oder -Methoden.  
2. **Integration Testing**: Test von Schnittstellen zwischen verschiedenen Modulen.  
3. **System Testing**: Test der gesamten Anwendung als Einheit.  
4. **Acceptance Testing**: Validierung der Anwendung aus der Sicht des Endbenutzers.  

**Beispiele für Tools**:  
- JUnit, [Selenium](../../../2_Unterrichtsressourcen/3_Tools/selenium/Readme.md), Postman, [JMeter](../../../2_Unterrichtsressourcen/3_Tools/jmeter/Readme.md), OWASP ZAP.,[Gitlab-Fuzz-Testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/index.html)

**Vorteile**:  
- Findet Laufzeitfehler (z. B. Speicherlecks, falsche Logik, Integration).  
- Testet die reale Nutzung der Software.  
- Unterstützt die Validierung gegen Benutzeranforderungen.

**Einschränkungen**:  
- Fehlererkennung erfolgt oft später im Entwicklungszyklus, was teurer ist.  
- Abhängig von einer lauffähigen Version der Software.  

---

### **Vergleich: Static Testing vs. Dynamic Testing**

| **Aspekt**              | **Static Testing**                      | **Dynamic Testing**                   |
|--------------------------|-----------------------------------------|---------------------------------------|
| **Ausführung der Software** | Nicht erforderlich                     | Erforderlich                          |
| **Testzeitpunkt**         | Frühzeitig im Entwicklungsprozess       | Später, nach Erstellung eines Builds  |
| **Testobjekte**           | Anforderungen, Code, Design, Dokumentation | Laufzeitverhalten und Funktionalität  |
| **Fehlerarten**           | Syntaxfehler, Logikfehler, Sicherheitslücken | Laufzeitfehler, Integrationsprobleme  |
| **Beispiele für Aktivitäten** | Code-Review, statische Codeanalyse       | Unit-, System- und Akzeptanztests     |
| **Beispiele für Tools**   | SonarQube, Checkmarx, Fortify           | Selenium, JUnit, Postman, JMeter      |

---


