# The Phoenix Project 

by Gene Kim, Kevin Behr and George Spafford

- Als Hörbuch in Englisch: 
    - Part 1: https://www.youtube.com/watch?v=1QHy8MmNOJ4 
    - Part2: https://www.youtube.com/watch?v=hUz5hxcDLAc
- Als Ebook auf Englisch:
    - die ersten 170 Seiten als PDF gratis: http://images.itrevolution.com/documents/The_Phoenix_Project_excerpt.pdf
    - https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business-ebook/dp/B078Y98RG8

- Als Ebook auf Deutsch:
    - https://www.orellfuessli.ch/shop/home/artikeldetails/A1035777807
    - https://www.thalia.de/shop/home/artikeldetails/A1035777807

