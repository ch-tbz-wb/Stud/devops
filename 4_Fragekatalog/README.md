# Fragekatalog 

[[_TOC_]]

---

**1. Was ist DevOps?**  
**Antwort:**  
DevOps ist eine Kultur, Philosophie und Sammlung von Praktiken, die darauf abzielt, die Zusammenarbeit zwischen Entwicklungs- und Betriebsteams zu fördern. Durch Automatisierung und kontinuierliche Prozesse werden Softwareentwicklung, Tests und Bereitstellung beschleunigt. DevOps zielt darauf ab, Software mit hoher Qualität und Geschwindigkeit zu liefern, wobei der Fokus auf Zusammenarbeit, kontinuierlichem Feedback und Integration liegt.

---

**2. Was bedeutet die DevOps-Kultur „CALMS“?**  
**Antwort:**  
CALMS steht für die fünf zentralen Säulen der DevOps-Kultur:  
- **C**ulture: Fokus auf Zusammenarbeit und offene Kommunikation.  
- **A**utomation: Automatisierung von Prozessen, um Fehler zu minimieren und Effizienz zu steigern.  
- **L**ean: Maximierung von Wertschöpfung durch Reduzierung von Verschwendung.  
- **M**easurement: Messung von Metriken zur kontinuierlichen Verbesserung.  
- **S**haring: Gemeinsames Lernen und Wissensaustausch zwischen Teams.

---

**3. Was sind die „Three Ways“ in DevOps?**  
**Antwort:**  
Die „Three Ways“ sind zentrale Prinzipien der DevOps-Philosophie:  
1. **Flow**: Optimierung des Workflows von der Entwicklung bis zur Bereitstellung.  
2. **Feedback Loops**: Schnelles und kontinuierliches Feedback zwischen Teams, um Fehler früh zu erkennen.  
3. **Continual Learning and Experimentation**: Ständige Verbesserung und Innovation durch Lernen aus Fehlern und Experimenten.

---

**4. Was sind die „Four Types of Work“ in DevOps?**  
**Antwort:**  
Die „Four Types of Work“ helfen, Aufgaben zu kategorisieren:  
1. **Business Projekte**: Aktivitäten, die direkt zur Wertschöpfung beitragen.  
2. **Interne Projekte**: Optimierung und Verbesserung interner Systeme und Prozesse.  
3. **Änderungsanforderungen**: Notwendige Anpassungen zur Verbesserung bestehender Systeme.  
4. **Ungeplante Arbeiten**: Notfallmaßnahmen und Reaktionen auf unerwartete Probleme.

---

**5. Was ist Continuous Integration (CI)?**  
**Antwort:**  
Continuous Integration (CI) ist die Praxis, Codeänderungen regelmäßig in ein zentrales Repository zu integrieren und automatisch zu testen. Dadurch werden Integrationsfehler frühzeitig erkannt, und die Qualität des Codes wird verbessert.

---

**6. Was bedeutet Continuous Delivery (CD)?**  
**Antwort:**  
Continuous Delivery (CD) ist ein Prozess, bei dem Software kontinuierlich so entwickelt wird, dass sie jederzeit bereit ist, in die Produktion überführt zu werden. Dabei wird sichergestellt, dass jede Codeänderung automatisch getestet und für den Einsatz vorbereitet wird.

---

**7. Was ist der Unterschied zwischen Continuous Delivery und Continuous Deployment?**  
**Antwort:**  
- **Continuous Delivery:** Software ist jederzeit bereit für die Produktion, aber die Bereitstellung erfolgt manuell.  
- **Continuous Deployment:** Jede Änderung, die Tests erfolgreich besteht, wird automatisch in die Produktion überführt.

---

**8. Was ist Infrastructure as Code (IaC)?**  
**Antwort:**  
Infrastructure as Code (IaC) ist die Praxis, Infrastruktur über Code zu definieren und zu verwalten. Dadurch können Umgebungen konsistent und reproduzierbar bereitgestellt und skaliert werden. Beispiele für IaC-Tools sind Terraform, Ansible und ARM Templates.

---

**9. Wie trägt Automatisierung zur Effizienz in DevOps bei?**  
**Antwort:**  
Automatisierung reduziert manuelle Arbeiten und minimiert Fehler. Dies umfasst das automatisierte Testen, die Bereitstellung von Software, die Verwaltung von Infrastruktur und das Monitoring. Automatisierte Prozesse ermöglichen schnellere, zuverlässigere und häufigere Releases.

---

**10. Was ist eine CI/CD-Pipeline?**  
**Antwort:**  
Eine CI/CD-Pipeline ist ein automatisierter Prozess, der die Schritte von der Codeintegration bis zur Bereitstellung umfasst. Sie enthält Phasen wie Build, Test, Integration, Deployment und Monitoring.

---

**11. Was ist der Unterschied zwischen DevOps und Agile?**  
**Antwort:**  
Agile ist ein Ansatz für die iterative Softwareentwicklung mit kurzen Sprints und schnellem Feedback. DevOps erweitert Agile, indem es den Fokus auf die Zusammenarbeit zwischen Entwicklung und Betrieb legt und die Automatisierung des gesamten Lebenszyklus betont, von der Entwicklung bis zur Bereitstellung und dem Betrieb.

---

**12. Welche Tools werden häufig in DevOps verwendet?**  
**Antwort:**  
- **Version Control:** Git, GitLab  
- **CI/CD:** Jenkins, Azure DevOps, GitLab CI  
- **Containerisierung:** Docker, Kubernetes  
- **Konfigurationsmanagement:** Ansible, Chef, Puppet  
- **Monitoring:** Prometheus, Grafana  

---

**13. Was ist „Shift Left“ in DevOps?**  
**Antwort:**  
„Shift Left“ bedeutet, dass Tests, Sicherheitsüberprüfungen und Qualitätskontrollen früh im Entwicklungsprozess durchgeführt werden, anstatt sie auf das Ende zu verschieben. Dies reduziert die Fehlerwahrscheinlichkeit und verbessert die Qualität der Software.

---

**14. Was ist Microservices-Architektur in DevOps?**  
**Antwort:**  
Eine Microservices-Architektur zerlegt eine Anwendung in kleine, unabhängige Dienste, die jeweils eine bestimmte Funktion ausführen. Diese Dienste können unabhängig voneinander entwickelt, bereitgestellt und skaliert werden. Microservices sind besonders nützlich in DevOps, da sie die Flexibilität und Geschwindigkeit erhöhen.

---

**15. Welche Rolle spielt Monitoring in DevOps?**  
**Antwort:**  
Monitoring überwacht Anwendungen und Infrastruktur in Echtzeit, um Leistungsprobleme, Ausfälle oder andere Anomalien zu identifizieren. Monitoring-Tools wie Prometheus und Grafana helfen dabei, schnell auf Probleme zu reagieren und die Stabilität der Systeme zu gewährleisten.

---

**16. Was sind die Vorteile von DevOps?**  
**Antwort:**  
- Schnellere Bereitstellung von Software  
- Höhere Flexibilität und Anpassungsfähigkeit  
- Verbesserte Zusammenarbeit zwischen Teams  
- Gesteigerte Qualität und Stabilität durch Automatisierung  
- Effizientere Nutzung von Ressourcen  

---

**17. Welche Herausforderungen gibt es bei der Einführung von DevOps?**  
**Antwort:**  
- Widerstand gegen Veränderungen in der Unternehmenskultur  
- Fehlende Automatisierungstools  
- Sicherheitsbedenken bei häufigen Releases  
- Komplexität beim Integrieren alter Systeme und Prozesse  

---

**18. Wie fördert DevOps Innovation?**  
**Antwort:**  
Durch die enge Zusammenarbeit und den ständigen Austausch von Feedback wird die Entwicklungszeit verkürzt, was es den Teams ermöglicht, schneller zu experimentieren und neue Ideen umzusetzen. Automatisierte Prozesse geben den Teams mehr Freiheit, sich auf kreative Lösungen zu konzentrieren.

---

Dieser Fragekatalog gibt eine umfassende Einführung in die wichtigsten DevOps-Konzepte, von Automatisierung und Pipelines bis hin zur Kultur und Philosophie der Zusammenarbeit.