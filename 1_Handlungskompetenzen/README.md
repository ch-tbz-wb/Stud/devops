# Handlungskompetenzen

## Kompetenzmatrix

| **Kompetenzband**                                           | **RP**                                                                                     |      **HZ**                                   |       **Novizenkompetenz**                                                                                                                                |    **Fortgeschrittene Kompetenz**                                                                                                                           | **Kompetenz professionellen Handelns**   | **Kompetenzexpertise** 
|--------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **a) Zusammenhang zwischen DevOps, Agile, Lean und ITSM**  |**B4.2 B4.3** **B7.1**|1| Grundkenntnisse über die Definitionen von DevOps, Agile, Lean und ITSM sowie ihre grundsätzlichen Ziele.                                                    | Versteht die Grundlagen und kann erklären, wie diese Methoden interagieren und sich ergänzen.                                                                | Kann Prozesse und Methoden aus allen Ansätzen in spezifischen Projekten anwenden und deren Wechselwirkungen bewerten.                                             | Entwickelt umfassende Strategien, die Agile, Lean, ITSM und DevOps nahtlos integrieren und gezielt auf Unternehmensziele ausrichten.                         |
| **b) Gezielte Kommunikation und Feedback Loops**          |**A1.7 A1.11** **B6.2**|2| Kennt die Bedeutung von Feedback-Loops und grundlegenden Kommunikationsmethoden im Team.                                                                    | Kann einfache Kommunikationsstrukturen und Feedback-Loops in der Praxis anwenden.                                                                            | Implementiert gezielte Kommunikations- und Feedbackprozesse, um die Zusammenarbeit zwischen mehreren Teams und Abteilungen zu verbessern.                        | Entwirft und steuert komplexe Kommunikationssysteme mit automatisierten Feedback-Loops in grossen, heterogenen Umgebungen.                                                                             |
| **c) DevOps Tools (GitHub/GitLab Pipelines, Jenkins, usw.)** |**B4.7**|3| Hat Grundkenntnisse in der Verwendung von GitHub/GitLab und Jenkins für Versionskontrolle und CI/CD.                                                        | Nutzt diese Tools aktiv und kann einfache Pipelines aufsetzen und verwalten.                                                                                 | Implementiert und verwaltet automatisierte Pipelines in komplexen Entwicklungsumgebungen und integriert mehrere Tools effizient.                                  | Optimiert und orchestriert Toolchains, um eine reibungslose Automatisierung, Integration und Bereitstellung über verschiedene Plattformen und Teams hinweg zu gewährleisten.                                                                                            |
| **d) Workflow-Optimierung und Git**                               |**B4.5 B7.2** **A1.2**| 4|Kennt die grundlegenden Prinzipien der Workflow-Optimierung, wie die Vermeidung von Engpässen.                                                              | Kann einfache Workflows in Teams optimieren, indem Engpässe erkannt und beseitigt werden.                                                                    | Entwickelt optimierte Workflows für die kontinuierliche Verbesserung in grossen Teams und Projekten.                                                              | Führt komplexe, organisationsweite Workflow-Optimierungsstrategien ein und sorgt für deren langfristige Anpassung und Weiterentwicklung.                                                                |
| **e) Kritische Erfolgsfaktoren und Schlüsselleistungsindikatoren (KPIs)** |**B5.3 B5.9** **B6.4**|5| Kennt einfache KPIs wie Release-Frequenz oder Fehlerquote.                                                                                                  | Versteht, wie verschiedene KPIs in der Praxis angewendet werden, um DevOps-Erfolg zu messen.                                                                 | Entwickelt und implementiert massgeschneiderte KPIs für spezifische DevOps-Initiativen und Projekte.                                                              | Schafft umfassende KPI-Systeme, die in Echtzeit den Erfolg und die Optimierung von DevOps-Prozessen unternehmensweit messen.                                                    |
| **f) Automatisierung, insbesondere Development- und Deployment-Pipelines** |**B4.6 B7.3**|6| Kennt die Grundlagen der Continuous Integration (CI) und Continuous Delivery (CD).                                                                          | Kann einfache Automatisierungsprozesse für CI/CD einrichten und verwalten.                                                                                   | Entwickelt und skaliert automatisierte Development- und Deployment-Pipelines in grossen Projekten oder Organisationen.                                            | Implementiert hochkomplexe, automatisierte Pipelines, die sicherstellen, dass sowohl Entwicklung als auch Bereitstellung vollständig automatisiert und skalierbar sind.                                                                                       |
| **g) Integration von Security-, Testing- und IaC-Tools**  |**B7.4 B5.4** **B6.5**|7| Kennt die grundsätzlichen Funktionen von Security-, Testing- und Infrastructure-as-Code (IaC)-Tools.                                                       | Kann einfache Integrationen von Sicherheits- und Test-Tools in eine CI/CD-Pipeline vornehmen.                                                                 | Integriert Security, Testing und IaC-Tools tiefgehend in automatisierte Pipelines und stellt sicher, dass Sicherheit und Compliance in allen Phasen gewährleistet sind. | Entwickelt umfassende Sicherheits-, Test- und Infrastrukturautomatisierungen, die nahtlos in CI/CD-Prozesse integriert und unternehmensweit standardisiert sind.   |
| **h) Skalierung von DevOps für Grossbetriebe**             |**B5.2** **B6.1** **A1.4**|8| Hat ein grundlegendes Verständnis davon, was es bedeutet, DevOps in einem kleinen Team anzuwenden.                                                          | Kennt die Herausforderungen und ersten Ansätze zur Skalierung von DevOps auf grössere Teams.                                                                  | Kann DevOps-Praktiken in grossen Organisationen implementieren und dabei technologische und kulturelle Skalierungsherausforderungen berücksichtigen.                | Entwickelt massgeschneiderte DevOps-Skalierungsstrategien, die die gesamte Unternehmenskultur, Technologie und Geschäftsziele berücksichtigen.                 


HZ = Hanlungsziele
RP = Rahmenlehrplan

# Rahmenlehrplan

## Allgemeine Handlungskompetenzen
 * B7 Technische Anforderungen analysieren und bestimmen
   * B7.1 Die ICT-Architektur zielorientiert (ICT Strategie) analysieren, beurteilen und bestimmen
   * B7.2 Geschäftsprozesse aus ICT-Sicht priorisieren, analysieren, spezifizieren und optimieren
   * B7.3 Technische Anforderungen aufnehmen und spezifizieren
   * B7.4 Den Einsatz von geschäftsrelevanten Systemen konzipieren
   * B7.5 Anforderungen für den Einsatz von Informatikmitteln spezifizieren


## Modulspezifische Handlungskompetenzen
 * B4 Entwicklungsmethoden zur Lösung von ICT-Problemen und Entwicklung von ICT-Innovationen zielführend einsetzen
   * B4.1 ICT-Innovation methodisch mitgestalten
   * B4.2 ICT-Problemstellungen unter Berücksichtigung vernetzten Denkens, Entwicklung neuer ICT-Lösungen und Anwendung aktueller Technologien identifizieren, analysieren und lösen
   * B4.3 Kreative situativ passende ICT-Lösungen für komplexe Probleme mit ineinandergreifenden Einflussgrössen entwickeln
   * B4.4 Geeignete Methoden der Entscheidungsfindung aufgrund der Kriterien- und Argumentationsanalyse anwenden
   * B4.5 Ganzheitliche ICT-Lösungsansätze unter Berücksichtigung von technischen, sozialen, gesellschaftlichen, ethischen, ökologischen und ökonomischen Aspekten entwickeln
   * B4.6 Aktuelle technologiebasierte Entwicklungswerkzeuge einsetzen
   * B4.7 Informationsquellen und Wissensnetzwerke kritisch reflexiv nutzen

 * B5 ICT-Projekte und Vorhaben planen, leiten, umsetzen und evaluieren 
   * B5.1 ICT-Projekte oder Vorhaben eigenständig bis zur Ausführungsreife planen und steuern
   * B5.2 Sich gegenseitig beeinflussende Faktoren berücksichtigen und Veränderungen antizipieren
   * B5.3 Die Erfolgsfaktoren, die Zusammenarbeit im Team, die Planung der Ressourcen, die Umweltbelastung und die Kostenkontrolle berücksichtigen
   * B5.4 Eine technische Risikoanalyse durchführen und die Ergebnisse in der Planung berücksichtigen
   * B5.5 Initiative und Kreativität bei der Gestaltung von Projekten sowie Durchsetzungsvermögen bei der Durchführung zeigen
   * B5.6 In interdisziplinären Projekten teamorientiert handeln
   * B5.7 Ausweichpläne, um auf potenzielle Umsetzungsprobleme zu reagieren, entwickeln
   * B5.8 Verhältnis zwischen Kosten und Terminen nach Vorgaben optimieren
   * B5.9 Dokumente, die die Überwachung des Projektfortschritts erleichtern, erstellen und pflegen3)
   * B5.10 ICT-Projekte termin- und budgetgerecht und in Übereinstimmung mit den ursprünglichen Anforderungen abschliessen

 * B6 Eine ICT-Organisationseinheit leiten
   * B6.1 Aus dem Unternehmensleitbild und der ICT-Strategie die Anforderungen und Rahmenbedingungen ableiten und diese in der technischen ICT-Organisationseinheit konkret umsetzen
   * B6.2 Ressourcen für eine ICT-Organisationseinheit planen und budgetieren, den Mitarbeitereinsatz organisieren und die Kommunikation zu den beteiligten Stakeholdern sicherstellen
   * B6.3 Den Informationsbedarf für Entscheidungssituationen aufgrund von Anspruchsgruppen bestimmen
   * B6.4 Informationstechnologien und -methoden, Marktinformationen und Umfeld (Konkurrenz, Forschung etc.) in der ICT beobachten und bewerten
   * B6.5 Risiken einer ICT-Abteilung analysieren und geeignete Massnahmen ableiten

 * A1 Unternehmens- und Führungsprozesse gestalten und verantworten
   * A1.2 Unternehmensprozesse überprüfen und zu Handen der Entscheidungsträger überzeugende Vorschläge zur Optimierung unterbreiten (Niveau: 3)
   * A1.4 Transformationsprozesse im Bereich neuer Technologien, neuer Geschäftsmodelle, Reorganisationen oder Geschäftsprozessinnovationen mitgestalten, mittragen und umsetzen (Niveau: 3) 
   * A1.7 Zusammenarbeit im Team gestalten, reflektieren und Regeln vereinbaren (Niveau: 3)
   * A1.11 Die Motivation im Team fördern und dieses zu Höchstleistungen befähigen (Niveau: 3)

## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen. Die Handlungskompetenzen werden in vier Anforderungsniveaus eingestuft.

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.
