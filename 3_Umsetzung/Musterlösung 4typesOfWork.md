### Mögliche Antworten zu den Fragen

#### 1. Business Projects (Geschäftsprojekte)
- **Was sind Business Projects und warum sind sie wichtig?**
  - Business Projects sind Initiativen, die direkt mit den geschäftlichen Zielen und Strategien eines Unternehmens verbunden sind. Sie können neue Produkte, Dienstleistungen oder Marktstrategien umfassen.
  - Sie sind wichtig, da sie das Wachstum und die Wettbewerbsfähigkeit eines Unternehmens fördern, indem sie neue Einnahmequellen schaffen oder bestehende optimieren.

- **Gebt zwei konkrete Beispiele für Business Projects.**
  - Einführung eines neuen Softwareprodukts auf dem Markt.
  - Entwicklung einer neuen Marketingkampagne zur Steigerung der Markenbekanntheit.

- **Welche Tools und Methoden werden verwendet, um Business Projects zu verwalten?**
  - Projektmanagement-Software wie Jira, Trello oder Microsoft Project.
  - Agile Methoden wie Scrum oder Kanban.

- **Wie beeinflussen Business Projects die Unternehmensziele und -strategien?**
  - Sie tragen zur Erreichung von Umsatzzielen bei.
  - Sie stärken die Marktposition durch Innovation und Anpassung an Kundenbedürfnisse.

#### 2. Internal IT Projects (Interne IT-Projekte)
- **Was sind Internal IT Projects und warum sind sie notwendig?**
  - Internal IT Projects sind Projekte, die auf die Verbesserung der internen IT-Infrastruktur und -Prozesse abzielen. Sie beinhalten Upgrades, Migrationen und die Einführung neuer IT-Systeme.
  - Sie sind notwendig, um die Effizienz und Sicherheit der IT-Umgebung zu erhöhen und die Betriebsstabilität zu gewährleisten.

- **Gebt zwei konkrete Beispiele für Internal IT Projects.**
  - Upgrade des Betriebssystems auf allen Unternehmenscomputern.
  - Implementierung eines neuen CRM-Systems (Customer Relationship Management).

- **Welche Herausforderungen gibt es bei der Verwaltung von Internal IT Projects?**
  - Koordination zwischen verschiedenen Abteilungen und Stakeholdern.
  - Minimierung von Betriebsunterbrechungen während der Implementierung.

- **Welche Best Practices und Tools unterstützen die Durchführung dieser Projekte?**
  - Nutzung von IT Service Management (ITSM) Tools wie ServiceNow.
  - Durchführung regelmäßiger Schulungen und Change Management Prozesse.

#### 3. Changes (Änderungen)
- **Was versteht man unter Changes in der IT?**
  - Changes sind jegliche Modifikationen an IT-Systemen oder -Prozessen, die implementiert werden, um Verbesserungen, Fehlerbehebungen oder Anpassungen vorzunehmen.

- **Warum sind regelmäßige Changes wichtig für die Systemstabilität?**
  - Sie sorgen dafür, dass Systeme auf dem neuesten Stand und sicher bleiben.
  - Regelmäßige Updates und Patches verhindern Sicherheitslücken und Systemausfälle.

- **Gebt zwei Beispiele für typische Changes, die in einem Unternehmen vorkommen.**
  - Sicherheitsupdates für Server und Netzwerkgeräte.
  - Anpassungen der Softwarekonfiguration zur Verbesserung der Leistung.

- **Wie werden Changes typischerweise verwaltet und dokumentiert?**
  - Durch ein Change Management System, das Anträge, Genehmigungen und Implementierungen von Changes verfolgt.
  - Dokumentation erfolgt oft in einem ITSM-Tool wie ServiceNow oder Jira.

#### 4. Unplanned Work (Ungeplante Arbeit)
- **Was ist Unplanned Work und wie entsteht sie?**
  - Unplanned Work sind Aufgaben, die unerwartet auftreten und nicht im Voraus geplant wurden. Sie entstehen oft durch Notfälle, Systemausfälle oder ungeplante Kundenanforderungen.

- **Welche Auswirkungen kann Unplanned Work auf den Geschäftsbetrieb haben?**
  - Es kann geplante Arbeiten und Projekte verzögern.
  - Es kann zu erhöhtem Stress und Ressourcenknappheit führen.

- **Gebt zwei Beispiele für Unplanned Work in einem IT-Kontext.**
  - Behebung eines plötzlichen Serverausfalls.
  - Reaktion auf einen unerwarteten Sicherheitsvorfall.

- **Welche Strategien gibt es, um Unplanned Work zu minimieren und zu bewältigen?**
  - Implementierung eines robusten Incident Management Systems.
  - Regelmäßige Überprüfung und Verbesserung der IT-Infrastruktur zur Reduzierung der Wahrscheinlichkeit ungeplanter Vorfälle.

### Ressourcen und Hilfsmittel
- **Buch**: "The Phoenix Project: A Novel About IT, DevOps, and Helping Your Business Win" von Gene Kim, Kevin Behr und George Spafford.
- **Online-Ressourcen**:
  - [DevOps Handbook](https://devops.com/handbook/)
  - Artikel und Fallstudien über DevOps-Praktiken (z.B. Atlassian, AWS, Red Hat)

Diese Antworten sollen eine Orientierung geben, wie sie die Fragen angehen könnten.