
[TOC]
# Umsetzung
 - Semester: 4
 - Bereich: Eine ICT-Organisationseinheit leiten

## Lektionen

* Präsenz: 40
* Virtuell: 40
* Selbststudium: 50

## Fahrplan

| **Lektionen** | **Selbststudium** | **Kompetenzbereich**                                                         | **Tools**                                      |
|---------------|-------------------|-------------------------------------------------------------------------------|------------------------------------------------|
| 10            | 6                 | a) Zusammenhang zwischen DevOps, Agile, Lean und ITSM                            | Jira, Confluence, Kanban-Boards                |
| 8             | 5                 | b) Gezielte Kommunikation und Feedback Loops                                     | Slack, Microsoft Teams, Zoom                   |
| 12            | 7                 | c) DevOps Tools (GitHub/GitLab Pipelines, Jenkins, usw.)                         | GitHub, GitLab, Jenkins                        |
| 10            | 7                 | d) Workflow-Optimierung                                                         | Jenkins, GitLab CI, CircleCI                   |
| 14            | 9                 | e) Kritische Erfolgsfaktoren und Schlüsselleistungsindikatoren (KPIs)            | Power BI, Grafana, Prometheus                  |
| 10            | 6                 | f) Automatisierung, insbesondere Development- und Deployment-Pipelines           | Jenkins, GitHub Actions, GitLab Pipelines      |
| 8             | 5                 | g) Integration von Security-, Testing- und IaC-Tools                             | Terraform, Ansible, SonarQube, OWASP ZAP   
| 8             | 5                 | h) Skalierung von DevOps für Grossbetriebe                                        | Kubernetes, Docker, AWS, Azure
| **Total: 80** | **Total: 50** | -    | - |                                                                                               

---

## Lernziele

 - **DevOps Ziele und Terminologie**
   - Zusammenhang zwischen DevOps, Agile, Lean and ITSM
   - Gezielte Kommunikation und *Feedback Loops*
    - Skalierung von DevOps für Grossbetriebe
 - **Vorteile von DevOps für Business und IT**
    - Kritische Erfolgsfaktoren und Schlüsselleistungsindikatoren
 - **Einsatz von CI/CD, Testing, Security und "The Three Ways"**
   - Workflow Optimierung 
   - Automatisierung, insbesondere Development- und Deployment-Pipelines
   - DevOps Tools ( GitHub/GitLab Pipelines, Jenkins, usw.)
   - Integration von anderen Security, Testing, IAC-Tools,...
 

 - **Praktische Übungen**

## Voraussetzungen

IaC1, IaC2, MAAS, AZURE, AWS, BPM, ITSM, FAAS

## Technik

Git, GitLab, GitHub, Vagrant, Puppet, Jenkins, API, SOA, OpenStack, Docker

## Methoden

- Grundlagen Theorie anhand von einem Buch
- Einige Uebungen auch von Azure Learning Path, github skills,....
- Eine Projektarbeit in Gruppen 50%

## Dispensation

Gemäss Reglement HF Lehrgang

## Schlüsselbegriffe

DevOps, Kultur, Agile, Lean, ITSM, IT Service Management, Service Design, Continuous Integration, Continuous Delivery

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Workshop, Gastreferenten, Einzel- und Gruppenarbeiten, Lernfragen, Präsentationen

## Lehrmittel

 - The Phoenix Project; By Gene Kim, Kevin Behr, George Spafford


