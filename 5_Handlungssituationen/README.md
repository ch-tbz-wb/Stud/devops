## Handlungsziele und Handlungssituationen 

### Handlungsziel 1:
Förderung einer vertrauensvollen, kollaborativen und lernorientierten Umgebung durch die Implementierung der CALMS-Prinzipien (Culture, Automation, Lean, Measurement, Sharing), um die Effizienz und Zusammenarbeit zwischen den DevOps-Teams zu verbessern.

**Typische Handlungssituation:**  
Ein DevOps-Team steht vor der Aufgabe, einen neuen Software-Release zu implementieren. Das Team nutzt automatisierte CI/CD-Pipelines (Automation) und führt regelmässige Retrospektiven durch, um kontinuierliche Verbesserungen zu identifizieren (Lean). Wichtige Leistungsindikatoren wie Deployment-Frequenz und Fehlerquote werden gemessen (Measurement). Offene Kommunikation und Wissensaustausch schaffen eine vertrauensvolle, kollaborative Arbeitsumgebung (Culture und Sharing).

---

### Handlungsziel 2:
Kennt den Nutzen der kontinuierlichen, toolunterstützten Entwicklung und Wartung (z.B. MVP, Kundenfeedback, Kosten/Nutzen, Qualität, Risikoreduktion).

**Typische Handlungssituation:**  
Ein Softwareentwicklungsteam arbeitet an einem neuen Produkt und evaluiert Tools und Techniken zur Optimierung der Entwicklung und Wartung. Sie besprechen den Nutzen von Minimal Viable Product (MVP), Kundenfeedback, Kosten-Nutzen-Analysen, Qualitätssicherung und Risikominderung, um geeignete Tools auszuwählen und einen nachhaltigen Entwicklungsprozess sicherzustellen.

---

### Handlungsziel 3:
Kennt Möglichkeiten zur Nutzung automatisierender Tools in der Entwicklungsumgebung (z.B. Linting, Build, Ausführen, Testen, Versionierung, Abhängigkeiten/Pakete).

**Typische Handlungssituation:**  
Ein Entwicklerteam erkennt, dass der manuelle Entwicklungsprozess fehleranfällig und ineffizient ist. Sie besprechen die Integration automatisierender Tools wie Linting, Build und Testen in ihren Workflow, um die Effizienz zu erhöhen, die Fehlerquote zu reduzieren und die Codequalität zu verbessern.

---

### Handlungsziel 4:
Kennt Praktiken zur featurebasierten Verwaltung von Sourcecode (z.B. GIT-Workflow, semantisches Versionieren).

**Typische Handlungssituation:**  
Ein Team bereitet die Implementierung eines neuen Features vor. Um den Sourcecode effizient zu verwalten, diskutieren sie den GIT-Workflow und die Nutzung von Branches für jede Funktion. Zudem wird das semantische Versionieren verwendet, um Versionsänderungen strukturiert zu dokumentieren und die Zusammenarbeit zu verbessern.

---

### Handlungsziel 5:
Kennt Möglichkeiten, Applikationen und Komponenten automatisiert zu builden, auszuführen und zu testen (z.B. Build-Management, Container, Pipelines).

**Typische Handlungssituation:**  
Ein agiles Team bereitet eine neue Softwareversion vor und diskutiert die Automatisierung des Build-, Ausführungs- und Testprozesses mit Tools wie Gradle oder npm. Zudem evaluieren sie die Verwendung von Containern und Pipelines, um Continuous Integration (CI) und Continuous Delivery (CD) effizient zu gestalten. Kann KPIs dazu ableiten.

---

### Handlungsziel 6:
Kennt Möglichkeiten, Applikationen automatisiert zu deployen und zu konfigurieren (z.B. Infrastructure as Code, Scripting, YAML, Vaults, SSH-Key).

**Typische Handlungssituation:**  
Ein DevOps-Team plant die Bereitstellung einer neuen Anwendungsversion und erforscht Automatisierungsmöglichkeiten durch Infrastructure as Code (IaC). Dabei nutzen sie Scripting und YAML zur Konfiguration und Vaults für das sichere Management von Zugangsdaten, um eine konsistente und sichere Bereitstellung zu gewährleisten.

---

### Handlungsziel 7:
Kann Security-, Testing- und Infrastructure-as-Code (IaC) Tools anwenden.

**Typische Handlungssituation:**  
Ein Unternehmen integriert Sicherheits- und Testverfahren frühzeitig in die CI/CD-Pipeline und nutzt IaC-Tools zur Automatisierung der Infrastruktur. Dies reduziert manuelle Fehler, verbessert die Sicherheit und minimiert Verzögerungen im Entwicklungszyklus.

---

### Handlungsziel 8:
Kennt Skalierung von DevOps für Grossbetriebe.

**Typische Handlungssituation:**  
Ein globales Unternehmen möchte DevOps in allen Abteilungen einführen. Die verschiedenen Teams nutzen jedoch unterschiedliche Tools und Methoden. Das Management entwickelt eine skalierbare DevOps-Strategie, um konsistente Prozesse und Tools unternehmensweit einzuführen und Silos zu vermeiden.
