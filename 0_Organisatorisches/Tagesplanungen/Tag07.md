# Tag 07

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-04
[Continuous integration (CI)](https://learn.microsoft.com/en-us/training/paths/az-400-implement-ci-azure-pipelines-github-actions/)

[Create a build pipeline with Azure Pipelines](https://learn.microsoft.com/en-us/training/modules/create-a-build-pipeline/8-build-multiple-configurations?pivots=ms-hosted-agents)

