# Tagesplanungen

| Link zu Tagesplanung | Datum | Actions | Themen |
| --- | --- | --- | --- |
| [Tag 01](Tag01.md) | 28.08.2024 | Modulübersicht <br> Warum DevOps ? <br> Was ist DevOps? <br> Devops Lifecycle | Einführung , Abgrenzungen , *Grundprinzipien von Devops* |
| [Tag 02](Tag02.md) | 30.08.2024 | Devops mit Gitlab | *Infrastruktur*, *Pipeline*, *Artefacts*, *Code-Repo* , *Continuous Integration CI*
| [Tag 03](Tag03.md) | 04.09.2024 | SDLC vs DevOps <br> Einführung Azure DevOps | *SDLC vs DevOps*, *Einführung Azure DevOps* |
| [Tag 04](Tag04.md) | 11.09.2024 | The Three Ways | *Kultur* , **The three ways** |
| [Tag 05](Tag05.md) | 13.09.2024 | Devops mit Gitlab | *Continuous Deployment CD*
| [Tag 06](Tag06.md) | 18.09.2024 | Git & Code-Repository |  *Grundlegendes Verständnis der Codeverwaltung*  |
| [Tag 07](Tag07.md) | 20.09.2024 |   Continuous integration (CI) | *Build*, *Packaging*,*Testing* |
| [Tag 08](Tag08.md) | 25.09.2024 | CALMS <br> Continuous deployment (CD) | *CALMS*, *Deployment*, *Release*, *Approval* | 
| [Tag 09](Tag09.md) | 02.10.2024 | Dependency und Artifact Management | *Software Inventar*, *Dependency Scans*, *Umgang mit  Abhängigkeiten*| 
| [Tag 10](Tag10.md) | 23.10.2024 | 4 Types of Work <br> Start Projekt | *4 Types of Work*, *Projekt Kickoff* |
| [Tag 11-19](./../../2_Unterrichtsressourcen/5_Projektauftrag/README.md) | 30.10.2024 <br> 01.11.2024 <br> 06.11.2024 <br> 11.11.2024 <br> 13.11.2024 <br> 18.11.2024 <br> 22.11.2024 <br>27.11.2024 | Projektarbeit | Weitere Kultur Themen: <br> *MVP*, *Agile Vorgehensweise - Agile Manifesto*, *Governance*, *DevSecOps* |
| [Tag 20](Tag20.md) | 04.12.2024 | Abschluss Projektarbeit |

*Themen werden eingeführt*
**Themen werden vertieft**

# Themenübersicht

![Themen](../../2_Unterrichtsressourcen/Tagesplanung_visuell.png)
---
