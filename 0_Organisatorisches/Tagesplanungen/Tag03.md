# Tag 03

**Gemäss Stundenplan: vor Ort oder Fernunterricht**

## Lektion 01-02

### SDLC vs DevOps
[SDLC vs DevOps](../../2_Unterrichtsressourcen/5_Aufträge/SDLC%20vs%20devops%20live%20cycle.md)

## Lektion 03-04

### Einführung Azure DevOps
[Azure learning path](https://learn.microsoft.com/en-us/training/paths/evolve-your-devops-practices/)